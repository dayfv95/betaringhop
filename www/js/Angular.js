var mainApp = angular.module( "ringMeNowMain", ['ngAnimate', 'ui.router', 'angular-carousel'] );
var BASE_URL = "https://beta.ringhop.com/mobile/users/";
var imageString ='';
var deviceOS = '';
var deviceVersion = '';
var uuid = ''
var imageStringVerify = '';
var forgotFlag = 0;
var loadCount = 0;
var subscribeArr = [];
document.addEventListener( "deviceready", onDeviceReady, false );
function onDeviceReady() {
    StatusBar.overlaysWebView(false);
    StatusBar.backgroundColorByName("black");
    pictureSource=navigator.camera.PictureSourceType;
    destinationType=navigator.camera.DestinationType;
    device = window.device;
    //var element = 'Device Name: ' + device.name + '<br />' + 'Device PhoneGap: ' + device.phonegap + '<br />' + 'Device Platform: ' + device.platform + '<br />' + 'Device UUID: ' + device.uuid + '<br />' + 'Device Version: ' + device.version + '<br />';
    deviceOS = device.platform;
    deviceVersion = device.version;
    uuid = device.uuid;        
}

function checkConnection() {
    var networkState = navigator.connection.type;
    loadCount = loadCount+1;
    var states = {};
    states[Connection.NONE]     = 'No network connection';
    if( loadCount == 1 ) {
        alert(states[networkState]);
    }
    
}

mainApp.controller( "loginBtnController", function( $scope, $http, $state, $location ) {
    var devicehight = $( window ).height();
    $scope.login = function ( $event ) {
        $state.transitionTo('home.login');
    };
   $('#home_div').css('height',devicehight);
} )

mainApp.controller( "loginformController", function( $scope, $http, $location, $state, $rootScope ) {
    var flagMenu = 0;
    $scope.forgot_msg = false;
    $scope.reg_msg = false;
    $scope.logout_msg = false;
    $rootScope.previousState;
    $rootScope.currentState;
    $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
        if( $rootScope.previousState == 'home.forgotpasswordnext' && $rootScope.currentState == 'home.login' ) {
            forgotFlag = 1;
        } else {
            forgotFlag = 0;
        }
    });

    if(forgotFlag == 1){
        $scope.forgot_msg = true;
    }

    
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css('min-height',height);
    if( $rootScope.previousState == 'home.settings' && $rootScope.currentState == 'home.login' ) {
        $scope.logout_msg = true;
        var rememberme_local = localStorage.getItem("rememberme_data");
        if( rememberme_local ) {
            var remember_data = JSON.parse(rememberme_local);
            $scope.email = remember_data.email;
            $scope.password = remember_data.password; 
        } else {
            $scope.email="";
            $scope.password=""; 
        }
    }
    $scope.remember = true;
    var flag = false;
    $scope.signIn = function ($event) {
        var inputEmail = $('#inputEmail3').val();
        var inputPasswd = $('#inputPassword3').val();
        var numberPattern = /^[A-Za-z0-9]{10,}$/;  
        $( '#inputEmail3' ).keyup(function (e) {
            if( $( '#email_status' ).html() == 'This field is required.' ) {
                $( '#email_status' ).html('Please enter at least 10 characters.');
            }
            if( $( '#email_status' ).html() == 'Please enter at least 10 characters.' ) {
                if( $('#inputEmail3').val().match( numberPattern ) ) {
                    $( '#email_status' ).html('');
                    $( '#email_label' ).removeAttr('style');
                    flag = 1;
                }
            }
        } );
        $( '#inputPassword3' ).keyup( function (e) {
            if( $( '#passwd_status' ).html() == 'This field is required.' ) {
                $( '#passwd_status' ).html('Please enter at least 6 characters.');
            }
            if( $( '#passwd_status' ).html() == 'Please enter at least 6 characters.' ) {
                if( $('#inputPassword3').val().length >= 6 ) {
                    $( '#passwd_status' ).html('');
                    $( '#pwd_label' ).removeAttr('style');
                    flag = 2;
                }
            }
        } );
        if(flag == 1 && flag == 2) {
            flag = true;
        }
        if( $( '#email_status' ).html() == '' && $( '#passwd_status' ).html() == '' ) {
            flag = true;
        }
        var getLoginData = {};
        if( inputEmail == '' && inputPasswd == '' ) {
            flag = false;
            $( '#email_status' ).html( 'This field is required.' );
            $( '#passwd_status' ).html( 'This field is required.' );
            $( '#email_label' ).css('color','red');
            $( '#pwd_label' ).css('color','red');
        } else {
            if( inputEmail == '' && inputPasswd != '' ) {
                flag = false;
                $( '#email_status' ).html( 'This field is required.' );
                $( '#email_label' ).css('color','red');
                if( inputPasswd.length < 6 ) {
                    $( '#passwd_status' ).html('Please enter at least 6 characters.');
                    $( '#pwd_label' ).css('color','red');
                }
            } else if( inputEmail != '' && inputPasswd == '' ) {
                flag = false;
                $( '#passwd_status' ).html( 'This field is required.' );
                $( '#pwd_label' ).css('color','red');
                if( !inputEmail.match( numberPattern ) ) {
                    $( '#email_status' ).html('Please enter at least 10 characters.');
                    $( '#email_label' ).css('color','red');
                }
            } else {
                flag = true;
            }
            if( flag == true ) {
                getLoginData['email'] = $scope.email;  
                getLoginData['password'] = $scope.password;
                getLoginData['responsetype'] = "json";
                getLoginData['signin_remember'] = $scope.remember;
                //console.log(JSON.stringify(getLoginData));
                $('.phpdebugbar-openhandler-overlay').show();
                $('.ui-loader').show();
                var responsePromise = $http.post( BASE_URL+"signin",JSON.stringify(getLoginData));
                responsePromise.success( function( data, status, headers, config ) {
                    //console.log(JSON.stringify(data));
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(data.status == 'success') {
                        $state.transitionTo('home.affter_login');
                        localStorage.setItem("userDetail",JSON.stringify(data));
                        if($scope.remember == true){
                            localStorage.setItem("rememberme_data",JSON.stringify(getLoginData));
                            localStorage.setItem("rememberme_flag",true);
                        } 
                    } else {
                        $('#error_text').html(data.message);
                    }
                });
                responsePromise.error(function (data, status, headers, config) {
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                    console.log( JSON.stringify( data ) ); 
                });
            }
        }
    };
    $scope.register = function ( $event ) {
        $state.transitionTo('home.register');
    };
    $scope.forgotPasswd = function ( $event ) {
        $state.transitionTo('home.forgotpassword');
    };
} )

//+++++++++++++++++++++++++++Register page controller+++++++++++++++++++++

mainApp.controller( "regFormController", function( $scope, $http, $timeout, $state ) {
    var flag = false;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css('min-height',height);
    $scope.contries = [
            {id:'1', value:'+1 United States'},
            {id:'1', value:'+1 Canada'}
        ];
    $scope.countryCode = $scope.contries[0];
    $scope.registerAction = function ( $event ) {
        $( '#stateSelect_status' ).html('');
        $( '#phone_status' ).html('');
        $( '#server_status' ).html('');
        var regPattern = /^\d+$/;
        var countrySelect = $( '#country_code' ).val();
        var phoneNumber = $( '#reg_phone_number' ).val();
        localStorage.setItem("regUserPhone",phoneNumber);

        $( '#reg_phone_number' ).keyup(function (e) {
            var withoutSpace = $('#reg_phone_number').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;

            if(  $( '#reg_phone_number' ).val().length == 0 ) {
                flag = false;
                $( '#phone_status' ).html('This field is required.');
            }
            if( $( '#reg_phone_number' ).val().length < 10 && $( '#reg_phone_number' ).val().length >0 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html('Please enter at least 10 characters.');
                } else {
                    $( '#phone_status' ).html('Please enter a valid number.');
                }
            }
            
            if( $( '#reg_phone_number' ).val().length > 10 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter no more than 10 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }
            if( $( '#reg_phone_number' ).val().length == 10 ) {
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html('');
                    $( '#reg_phone_label' ).removeAttr('style');
                    flag = true;
                }  else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }
            if( $('#reg_phone_number').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#phone_status' ).html('This field is required.');
                    $('#reg_phone_label').css('color','red');
                }
            }
        } );
        
        if( $( '#phone_status' ).html() == '' ) {
            flag = true;
        }
        
        if( phoneNumber != '' ) {
            var withoutSpace = $('#reg_phone_number').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( phoneNumber.length < 10 ) {
                flag = false;
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 10 characters.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                } else {
                    if(  withoutSpaceLength == 0 ) {
                        flag = false;
                        $( '#phone_status' ).html('This field is required.');
                        $( '#reg_phone_label' ).css( 'color', 'red' );
                    } else {
                        $( '#phone_status' ).html( 'Please enter a valid number.' );
                        $( '#reg_phone_label' ).css( 'color', 'red' );
                    }
                }
                
            }
            if( phoneNumber.length > 10 ) {
                flag = false;
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter no more than 10 characters.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                }
            }

            if( phoneNumber.length == 10 ) {
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html('');
                    $( '#reg_phone_label' ).removeAttr('style');
                    flag = true;
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }

            if( flag == true ) {
                var country_id = $scope.countryCode.id;
                var getRegData = {};
                getRegData['country_code'] = country_id;
                getRegData['responsetype'] = "json";
                getRegData['phonenumber'] = $scope.phoneNumber;
                //console.log(JSON.stringify(getRegData));
                $('.phpdebugbar-openhandler-overlay').show();
                $('.ui-loader').show();
                var responsePromise = $http.post(BASE_URL+"register",JSON.stringify(getRegData));
                responsePromise.success( function( data, status, headers, config ) {
                    //console.log(JSON.stringify(data));
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(data.status == 'success') {
                        responseHtml = data.message;
                        $state.transitionTo('home.pinsent');
                    } else {
                        $('#server_status').html(data.message);
                    }
                });
                responsePromise.error(function (data, status, headers, config) {
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                    console.log( JSON.stringify( data ) ); 
                });
            }
        } else {
            $( '#phone_status' ).html('This field is required.');
            $( '#reg_phone_label' ).css('color','red');
        }
          
    };
} )

//+++++++++++++++++++++++++++Forgot Password page controller+++++++++++++++++++++

mainApp.controller( "forgotpwdController", function( $scope, $http, $timeout, $state ) {
    var flag = false; 
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css('min-height',height);
    $scope.forgotPwdAction = function ( $event ) {
        $('#number_error').html('');
        $('#forgot_status').html('');
        var pattern = /^[A-Za-z0-9]{10,}$/;  
        var mobile_number = $('#inputEmail5').val();
        $( '#inputEmail5' ).keyup(function (e) {
            if( $( '#number_error' ).html() == 'This field is required.' ) {
                $( '#number_error' ).html('Please enter at least 10 characters.');
            }
            if( $( '#number_error' ).html() == 'Please enter at least 10 characters.' ) {
                if( $('#inputEmail5').val().match( pattern ) ) {
                    $( '#number_error' ).html('');
                    $( '#phone_label' ).removeAttr('style');
                    flag = true;
                }
            }
        } );
        if($( '#number_error' ).html() == '') {
            flag = true;
        }
        if( mobile_number != '' ) {
            if( mobile_number.length < 10) {
                flag = false;
                $('#number_error').html('Please enter at least 10 characters.');
                $( '#phone_label' ).css('color','red');
            }
            if( flag == true ) {
                localStorage.setItem("UserPhone_FG",mobile_number);
                var getForgotData = {};
                getForgotData['phonenumber'] = $scope.mobileNumber;
                getForgotData['responsetype'] = "json";
                //console.log(JSON.stringify( getForgotData ));
                $('.phpdebugbar-openhandler-overlay').show();
                $('.ui-loader').show();
                var responsePromise = $http.post(BASE_URL+"forgotpassword",JSON.stringify(getForgotData));
                responsePromise.success( function( data, status, headers, config ) {
                    //console.log(JSON.stringify( data ));
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if( data.status == 'success' ) {
                        $state.transitionTo('home.pinConfirmForgot');
                    } else {
                        $('#forgot_status').html( data.message );
                    }
                } );
                responsePromise.error( function ( data, status, headers, config ) {
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                    console.log( JSON.stringify( data ) ); 
                } );
            }
        } else {
            $('#number_error').html('This field is required.');
            $( '#phone_label' ).css('color','red');
        }
    };
} )

//+++++++++++++++++++++++++++Reset Password after forgot page controller+++++++++++++++++++++

mainApp.controller( "resetpwdController", function( $scope, $http, $timeout, $state, $rootScope ) {
    var flag = true;
    
    $scope.resetForgotPwd = function( $event ) {
        $('#reset_status').html('');
        var password = $scope.password;
        var confirm_passwd = $scope.confirmpassword;
        $( '#passwd' ).keyup(function (e) {
            if( $( '#passwd_error' ).html() == 'This field is required.' ) {
                $( '#passwd_error' ).html('Please enter at least 6 characters.');
            }
            if( $( '#passwd_error' ).html() == 'Please enter at least 6 characters.' ) {
                if( $('#passwd').val().length >= 6 ) {
                    $( '#passwd_error' ).html('');
                    $( '#passwd_label' ).removeAttr('style');
                    flag = 1;
                }
            }
        } );
        $( '#con_passwd' ).keyup( function (e) {
            if( $( '#con_passwd_error' ).html() == 'This field is required.' ) {
                $( '#con_passwd_error' ).html('Please enter at least 6 characters.');
            }
            if( $( '#con_passwd_error' ).html() == 'Please enter at least 6 characters.' ) {
                if( $('#con_passwd').val().length >= 6 ) {
                    $( '#con_passwd_error' ).html('');
                    $( '#con_passwd_label' ).removeAttr('style');
                    flag = 2;
                }
            }
        } );
        if(flag == 1 && flag == 2) {
            flag = true;
        }
        if($( '#con_passwd_error' ).html() && $( '#passwd_error' ).html() == ''){
            flag = true;
        }
        if( $('#passwd').val() == '' && $('#con_passwd').val() == '' ) {
            $('#passwd_error').html('This field is required.');
            $('#con_passwd_error').html('This field is required.');
            $( '#passwd_label' ).css('color','red');
            $( '#con_passwd_label' ).css('color','red');
        } else {
            if( $('#passwd').val() == '' && $('#con_passwd').val() != '' ) {
                flag = false;
                $('#passwd_error').html('This field is required.');
                $( '#passwd_label' ).css('color','red');
                if( $('#con_passwd').val().length < 6 ) {
                    $( '#con_passwd_error' ).html('Please enter at least 6 characters.');
                    $( '#con_passwd_label' ).css('color','red');
                }
            } else if( $('#passwd').val() != '' && $('#con_passwd').val() == '' ) {
                flag = false;
                $( '#con_passwd_error' ).html( 'This field is required.' );
                $( '#con_passwd_label' ).css('color','red');
                if( $('#passwd').val().length < 6 ) {
                    $( '#passwd_error' ).html('Please enter at least 6 characters.');
                    $( '#passwd_label' ).css('color','red');
                }
            } else {
                flag = true;
            }
            if( password == confirm_passwd ) {
                if ( flag == true ) {
                    var user_phone = localStorage.getItem('UserPhone_FG');
                    var getResetData = {};
                    getResetData['password'] = password;
                    getResetData['confirmpassword'] = confirm_passwd;
                    getResetData['phonenumber'] = user_phone;
                    getResetData['responsetype'] = 'json';
                    //console.log(JSON.stringify(getResetData));
                    $('.phpdebugbar-openhandler-overlay').show();
                    $('.ui-loader').show();
                    var responsePromise = $http.post(BASE_URL+"resetpassword",JSON.stringify(getResetData));
                    responsePromise.success( function( data, status, headers, config ) {
                       // console.log(JSON.stringify(data));
                        $('.phpdebugbar-openhandler-overlay').hide();
                        $('.ui-loader').hide();
                        if( data.status == 'success' ) {
                            //$rootScope.$broadcast("forgotStatus", data.message);
                            $state.transitionTo('home.login');
                        } else {
                            $('.phpdebugbar-openhandler-overlay').hide();
                            $('.ui-loader').hide();
                            $('#reset_status').html( data.message );
                        }
                    } );
                    responsePromise.error( function ( data, status, headers, config ) {
                        $('.phpdebugbar-openhandler-overlay').hide();
                        $('.ui-loader').hide();
                        if(navigator.connection.type == Connection.NONE) {
                            checkConnection();
                        }
                        console.log( JSON.stringify( data ) ); 
                    } );
                }
            } else {
                flag = false;
                $('#reset_status').html( 'password is not matched' );
            }
        }
    };
 } )

//+++++++++++++++++++++++++++Pin Confirm for register page controller+++++++++++++++++++++

mainApp.controller( "pinMatchController", function( $scope, $http, $timeout, $state) {
    var flag = false;
    var pinPattern = /^[0-9]+$/;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css('min-height',height);
    $scope.pinMatchAction = function ( $event ) {
        $('#pin_error').html('');
        $('#pinMatch_status').html('');
        var pin_number = $('#inputEmail6').val();
        
        $( '#inputEmail6' ).keyup(function (e) {
            var withoutSpace = $('#inputEmail6').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $( '#pin_error' ).html() == 'This field is required.' ) {
                $( '#pin_error' ).html('Please enter at least 4 characters.');
            }
            if( withoutSpaceLength < 4 ) {
                flag = false;
                $( '#pinMatch_status' ).html('');
                if( withoutSpaceLength == 0 ) {
                    $( '#pin_error' ).html('This field is required.');
                    $('#pin_label').css('color','red');
                } else {
                    $('#pin_error').html('Please enter at least 4 characters.');
                    $('#pin_label').css('color','red');
                }
            } 
            if( withoutSpaceLength > 4 ) {
                flag = false;
                $( '#pin_error' ).html('Please enter no more than 4 characters.');
                $('#pin_label').css('color','red');
                $( '#pinMatch_status' ).html('');
            }
            if( withoutSpaceLength == 4 ) {
                $( '#pin_error' ).html('');
                $( '#pin_label' ).removeAttr('style');
                if( !$('#inputEmail6').val().match( pinPattern ) ) {
                    flag = false;
                } else{
                    flag = true;
                }
            }
            
            if( $('#inputEmail6').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#pin_error' ).html('This field is required.');
                    $('#pin_label').css('color','red');
                } else if( withoutSpaceLength < 4) {
                    $( '#pin_error' ).html('Please enter at least 4 characters.');
                    $('#pin_label').css('color','red');
                } else if( withoutSpaceLength > 4) {
                    $( '#pin_error' ).html('Please enter no more than 4 characters.');
                    $('#pin_label').css('color','red');
                }
            }
        } );
        if( $( '#pin_error' ).html() == '') {
            flag = true;
        }
        if( pin_number != '' ) {
            var withoutSpace = $('#inputEmail6').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength < 4 ) {
                $( '#pinMatch_status' ).html('');
                flag = false;
                $('#pin_label').css('color','red');
                if( withoutSpaceLength == 0 ) {
                    $( '#pin_error' ).html('This field is required.');
                } else {
                    $('#pin_error').html('Please enter at least 4 characters.');
                }
            } 
            if( withoutSpaceLength > 4 ){
                $( '#pinMatch_status' ).html('');
                flag = false;
                $( '#pin_error' ).html('Please enter no more than 4 characters.');
                $('#pin_label').css('color','red');
            }
            if( withoutSpaceLength == 4 ) {
                $( '#pin_error' ).html('');
                $( '#pin_label' ).removeAttr('style');
                if( !$('#inputEmail6').val().match( pinPattern ) ) {
                    $( '#pinMatch_status' ).html('The pin must be a number.');
                    flag = false;
                } else{
                    flag = true;
                }
            }
            if( flag == true ) {
                var user_phone_stored = localStorage.getItem('regUserPhone');
                localStorage.setItem("regUserPin",$scope.pinNumber);
                var getPinData = {};
                getPinData['pin'] = $scope.pinNumber;
                getPinData['responsetype'] = 'json';
                getPinData['phonenumber'] = user_phone_stored;
               // console.log( JSON.stringify( getPinData ) ); 
                $('.phpdebugbar-openhandler-overlay').show();
                $('.ui-loader').show();
                var responsePromise = $http.post(BASE_URL+"verifypin",JSON.stringify(getPinData));
                responsePromise.success( function( data, status, headers, config ) {
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if( data.status == 'success' ) {
                        localStorage.setItem("user_userid",data.userid);
                        $state.transitionTo('home.register_step2');
                    } else {
                        $('#pinMatch_status').html( data.message );
                    }
                } );
                responsePromise.error( function ( data, status, headers, config ) {
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                    console.log( JSON.stringify( data ) ); 
                } );
            }
        } else {
            $('#pin_error').html('This field is required.');
            $('#pin_label').css('color','red');
        }
    };
} )

//+++++++++++++++++++++++++++Pin Confirm for Forgot page controller+++++++++++++++++++++

mainApp.controller( "pinMatchFGController", function( $scope, $http, $timeout, $state, $rootScope) {
    var flag = false;
    var pinPattern = /^[0-9]+$/;
    $scope.pinMatchFGAction = function ( $event ) {
        $('#pinFG_error').html('');
        $('#pin_send_status').remove();
        $('#pinMatchFG_status').html('');
        var pin_number = $('#inputEmail7').val();
        var getPinData = {};
        $( '#inputEmail7' ).keyup(function (e) {
            var withoutSpace = $('#inputEmail7').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $( '#pinFG_error' ).html() == 'This field is required.' ) {
                $( '#pinFG_error' ).html('Please enter at least 4 characters.');
            }
            if( withoutSpaceLength < 4 ) {
                flag = false;
                $('#pinFG_error').html('Please enter at least 4 characters.');
                $('#pinnumber_label').css('color','red');
                $( '#pinMatchFG_status' ).html('');
            } 
            if( withoutSpaceLength > 4 ) {
                flag = false;
                $( '#pinFG_error' ).html('Please enter no more than 4 characters.');
                $('#pinnumber_label').css('color','red');
                $( '#pinMatchFG_status' ).html('');
            }
            if( withoutSpaceLength == 4 ) {
                $( '#pinFG_error' ).html('');
                $( '#pinnumber_label' ).removeAttr('style');
                if( !$('#inputEmail7').val().match( pinPattern ) ) {
                    flag = false;
                } else {
                    flag = true;
                }
            }
            if( $('#inputEmail7').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#pinFG_error' ).html('This field is required.');
                    $('#pinnumber_label').css('color','red');
                } else if( withoutSpaceLength < 4) {
                    $( '#pinFG_error' ).html('Please enter at least 4 characters.');
                    $('#pinnumber_label').css('color','red');
                } else if( withoutSpaceLength > 4) {
                    $( '#pinFG_error' ).html('Please enter no more than 4 characters.');
                    $('#pinnumber_label').css('color','red');
                }
                
            }
        } );
        if( $( '#pinFG_error' ).html() == '') {
            flag = true;
        }
        if( pin_number != '' ) {
            var withoutSpace = $('#inputEmail7').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $('#inputEmail7').val().indexOf(' ') >= 0 ) {
                $('#pinnumber_label').css('color','red');
                if( withoutSpaceLength == 0 ) {
                    $( '#pinFG_error' ).html('This field is required.');
                } 
            }
            if( withoutSpaceLength > 4 ){
                $( '#pinMatchFG_status' ).html('');
                flag = false;
                $( '#pinFG_error' ).html('Please enter no more than 4 characters.');
                $('#pinnumber_label').css('color','red');
            }
            if( withoutSpaceLength == 4 ) {
                $( '#pinFG_error' ).html('');
                $( '#pinnumber_label' ).removeAttr('style');
                if( !$('#inputEmail7').val().match( pinPattern ) ) {
                    $( '#pinMatchFG_status' ).html('The pin must be a number.');
                    flag = false;
                } else {
                    flag = true;
                }
            }
            if( withoutSpaceLength < 4) {
                $( '#pinMatchFG_status' ).html('');
                flag = false;
                $('#pinFG_error').html('Please enter at least 4 characters.');
                $('#pinnumber_label').css('color','red');
            }

            if( flag == true ) {
                var user_phone_stored = localStorage.getItem('UserPhone_FG');
                localStorage.setItem("regUserPin",$scope.pinNumber);
                getPinData['pin'] = $scope.pinNumber;
                getPinData['responsetype'] = 'json';
                getPinData['phonenumber'] = user_phone_stored;
                $('.phpdebugbar-openhandler-overlay').show();
                $('.ui-loader').show();
               // console.log(JSON.stringify( getPinData ) );
                var responsePromise = $http.post(BASE_URL+"verifypin",JSON.stringify(getPinData));
                responsePromise.success( function( data, status, headers, config ) {
                    //console.log(JSON.stringify( data ) );
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if( data.status == 'success' ) {
                        $state.transitionTo('home.forgotpasswordnext');
                    } else {
                        $('#pinMatchFG_status').html( data.message );
                    }
                } );
                responsePromise.error( function ( data, status, headers, config ) {
                    $('.phpdebugbar-openhandler-overlay').hide();
                    $('.ui-loader').hide();
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                    console.log( JSON.stringify( data ) ); 
                } );
            }
        } else {
            $('#pinFG_error').html('This field is required.');
            $('#pinnumber_label').css('color','red');
        }
    };
} )

//+++++++++++++++++++++++++++Registration step2 page controller+++++++++++++++++++++

mainApp.controller( "regStepTwoController", function( $scope, $http, $timeout, $state ) {
    $('#register2Status').html('');
    var flag = false;
    var count = 0;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,3})+$/;  
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css('min-height',height);
    $scope.registerSubmit = function( $event ) {
        var withoutSpace = $('#regPassword7').val().replace(/ /g,"");
        var withoutSpaceLength = withoutSpace.length;
        var user_phone_stored = localStorage.getItem('regUserPhone');
        var new_val = "";
        var regFormDiv = document.getElementById('register_form2');
        var input_id = '';
        var elementsLength = parseInt(regFormDiv.getElementsByTagName('input').length);
        $('#gender_error').html('');
        $('#gender_label').removeAttr('style');
        if($scope.agreement == true) {
            $('#agreement').val(1);
        }  
        for (var i=0; i<elementsLength; i++) {
            input_id = regFormDiv.getElementsByTagName('input')[i].id;
            new_val = regFormDiv.getElementsByTagName('input')[i].value;
            $("label[for='"+input_id+"']").removeAttr('style');
            $('#terms_error').html('');
            $('#terms_label').removeAttr('style');
            document.getElementsByTagName("span")[i].innerHTML = '';
            if(new_val == '') {
                document.getElementsByTagName("span")[i].innerHTML = 'This field is required.';
                $("label[for='"+input_id+"']").css('color','red');
            }
            
            if( i == elementsLength-1 ){
                if($('#agreement').val() == 0 || $scope.agreement == false){
                    $('#terms_error').html('This field is required.');
                    $('#terms_label').css('color','red');
                } 
            }
        }

        if($('#regGender7').val() == '') {
            $('#gender_error').html('This field is required.');
            $('#gender_label').css('color','red');
        }
        
        $( '#username_input' ).keyup(function (e) {
            if( $( '#username_error' ).html() == 'This field is required.' ) {
                $( '#username_error' ).html('Please enter at least 5 characters.');
            }
            if( $('#username_input').val().length >= 5 ) {
                if( $('#username_input').val().length <= 12 ) {  
                    $( '#username_error' ).html('');
                    $( '#username_label' ).removeAttr('style');
                    
                } else {
                    $( '#username_error' ).html('Please enter no more than 12 characters.');
                    $('#username_label').css('color','red');
                }
            } else {
                $( '#username_error' ).html('Please enter at least 5 characters.');
                $('#username_label').css('color','red');
            }
            
            if( ( $('#username_input').val().indexOf(' ') >= 0 ) || ( /^[a-zA-Z0-9- ]*$/.test( $( '#username_input' ).val() ) == false) ){
                $( '#username_error' ).html('The username may only contain letters and numbers.');
                $( '#username_label' ).css('color','red');
            }
        } );
        
        $( '#email_input' ).keyup(function (e) {
            if( $( '#email_error' ).html() == 'This field is required.' ) {
                $( '#email_error' ).html('Please enter a valid email address.' );
            }
            if( $( '#email_error' ).html() == 'Please enter a valid email address.' ) {
                if( $('#email_input').val().match(mailformat) ) {
                    $( '#email_error' ).html('');
                    $( '#email_label' ).removeAttr('style');
                }
            }
        } );

        $( '#regPassword7' ).keyup(function (e) {
            var withoutSpace = $('#regPassword7').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $( '#password2_error' ).html() == 'This field is required.' ) {
                $( '#password2_error' ).html('Please enter at least 6 characters.');
            }
            
            if( withoutSpaceLength >= 6 ) {
                if( withoutSpaceLength <= 15 ) {  
                    $( '#password2_error' ).html('');
                    $( '#password2_label' ).removeAttr('style');
                   
                } else {
                    $( '#password2_error' ).html('Please enter no more than 15 characters.');
                    $('#password2_label').css('color','red');
                }
            } else {
                $( '#password2_error' ).html('Please enter at least 6 characters.');
                $('#password2_label').css('color','red');
            }

            if( $('#regPassword7').val().indexOf(' ') >= 0 ) {
                $('#password2_label').css('color','red');
                if( withoutSpaceLength == 0 ) {
                    $( '#password2_error' ).html('This field is required.');
                } else if( withoutSpaceLength > 15 ) {
                    $( '#password2_error' ).html('Please enter no more than 15 characters.');
                } else if( withoutSpaceLength < 6 ) {
                    $( '#password2_error' ).html('Please enter at least 6 characters.');
                }
                
            }
            
        } );

        $( '#regGender7' ).change(function() {
            if( $('#regGender7').val() == 'M' || $('#regGender7').val() =='F') {
                $( '#gender_error' ).html('');
                $( '#gender_label' ).removeAttr('style');
            } else {
                $( '#gender_error' ).html('This field is required.');
                $('#gender_label').css('color', 'red');
            }
        } );
        
        if( $('#username_input').val() != '' && $('#username_input').val().length < 5 ) {
            $( '#username_error' ).html('Please enter at least 5 characters.');
            $( '#username_label' ).css('color', 'red');
        }

        if( $('#username_input').val() != '' && $('#username_input').val().length > 12 ) {
            $( '#username_error' ).html('Please enter no more than 12 characters.');
            $( '#username_label' ).css('color', 'red');
        }

        if( ( $('#email_input').val() != '' ) && !( $('#email_input').val().match(mailformat) ) ){
            $( '#email_error' ).html('Please enter a valid email address.');
            $( '#email_label' ).css('color', 'red');
        }

        if( $('#regPassword7').val() != '' && withoutSpaceLength < 6 ) {
            $( '#password2_error' ).html( 'Please enter at least 6 characters.' );
            $( '#password2_label' ).css( 'color', 'red' );
        }

        if( $('#regPassword7').val() != '' && withoutSpaceLength > 15 ) {
            $( '#password2_error' ).html( 'Please enter no more than 15 characters.' );
            $( '#password2_label' ).css( 'color', 'red' );
        }

        if( $('#regPassword7').val().indexOf(' ') >= 0 ) {
            $('#password2_label').css('color','red');
            var withoutSpace = $('#regPassword7').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#password2_error' ).html('This field is required.');
            } else if( withoutSpaceLength > 15 ) {
                $( '#password2_error' ).html('Please enter no more than 15 characters.');
            } else if( withoutSpaceLength < 6 ) {
                $( '#password2_error' ).html('Please enter at least 6 characters.');
            }
            
        }
        if ( $( '#username_error' ).html() == '' && $( '#email_error' ).html() == '' && $( '#password2_error' ).html() == '' && $( '#gender_error' ).html() == '' && $( '#terms_error' ).html() == '' ) {
            flag = true;
        }
        var getregStepTwoData = {};
        getregStepTwoData['username'] = $scope.register_username;
        getregStepTwoData['email'] = $scope.register_email;
        getregStepTwoData['password'] = $scope.register_password;
        getregStepTwoData['gender'] = $scope.register_gender;
        getregStepTwoData['register_optout'] = $scope.agreement;
        getregStepTwoData['phonenumber'] = user_phone_stored;
        getregStepTwoData['responsetype'] = 'json';
       //console.log( JSON.stringify( getregStepTwoData ) ); 
        if ( flag == true ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            var responsePromise = $http.post(BASE_URL+"registernext",JSON.stringify(getregStepTwoData));
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( JSON.stringify( data ) ); 
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( data.status == 'success' ) {
                    localStorage.setItem("userDetail",JSON.stringify(data));
                    $state.transitionTo('home.affter_login');
                } else {
                    $('#register2Status').html( data.message );
                }
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
                console.log( JSON.stringify( data ) ); 
            } );
        }
    };
} )

//+++++++++++++++++++++++++++Pin sent Text page controller+++++++++++++++++++++

mainApp.controller( "pinSentController", function( $scope, $http, $timeout, $state ) {
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css('min-height',height);
} )