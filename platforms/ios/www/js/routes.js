mainApp.config(function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
        
        .state('home', {
            url: '/home',
            templateUrl: 'home.html',
            controller: 'loginBtnController'
        })
        
       .state('home.login', {
            url: '/login',
            templateUrl: 'home-login.html',
            controller: 'loginformController'
        })
        
        .state('home.register', {
            url: '/register',
            templateUrl: 'home-register.html',
            controller: 'regFormController'
        })

        .state('home.forgotpassword', {
            url: '/forgotpassword',
            templateUrl: 'home-forgotpassword.html',
            controller: 'forgotpwdController'
        })

        .state('home.forgotpasswordnext', {
            url: '/forgotpasswordnext',
            templateUrl: 'home-forgotpasswordnext.html',
            controller: 'resetpwdController'
        })

        .state('home.pinConfirm', {
            url: '/pinConfirm',
            templateUrl: 'home-pinConfirm.html',
            controller: 'pinMatchController'
        })

        .state('home.pinConfirmForgot', {
            url: '/pinConfirmForgot',
            templateUrl: 'home-pinConfirmForgot.html',
            controller: 'pinMatchFGController'
        })

        .state('home.register_step2', {
            url: '/register_step2',
            templateUrl: 'home-register_step2.html',
            controller: 'regStepTwoController'
        })

        .state('home.affter_login', {
            url: '/affter_login',
            templateUrl: 'home-affter_login.html',
            controller: 'dashBoardController'
        })

        .state('home.phonesettings', {
            url: '/phonesettings',
            templateUrl: 'home-phonesettings.html',
            controller: 'availabilityController'
        })

        .state('home.resetpassword', {
            url: '/resetpassword',
            templateUrl: 'home-resetpassword.html',
            controller: 'changepwdController'
        })

        .state('home.editProfile', {
            url: '/editProfile',
            templateUrl: 'home-editProfile.html',
            controller: 'editProfileController'
        })

        .state('home.messages', {
            url: '/messages',
            templateUrl: 'home-messages.html',
            controller: 'recentMessageController'
        })

        .state('home.reply', {
            url: '/reply:messageId',
            templateUrl: 'home-reply.html',
            controller: 'replyController'
            /*resolve:{
                messageId: ['$stateParams', function($stateParams){
                    return $stateParams.messageId;
                }]
            }*/
        })

        .state('home.settings', {
            url: '/settings',
            templateUrl: 'home-settings.html',
            controller: 'settingsController'
        })

        .state('home.pinsent', {
            url: '/pinsent',
            templateUrl: 'home-pinsent.html',
            controller: 'pinSentController'
        })

        .state('home.upgrade', {
            url: '/upgrade',
            templateUrl: 'home-upgrade.html',
            controller: 'proUpgradeController'
        })

        .state('home.paymentoption', {
            url: '/paymentoption',
            templateUrl: 'home-paymentoption.html',
            controller: 'paymentoptionController'
        })

        .state('home.learnmore', {
            url: '/learnmore',
            templateUrl: 'home-learnmore.html',
            controller: 'learnmoreController'
        })

        .state('home.prodashboard', {
            url: '/prodashboard',
            templateUrl: 'home-prodashboard.html',
            controller: 'prodashboardController'
        })

        .state('home.acceptterms', {
            url: '/acceptterms',
            templateUrl: 'home-acceptterms.html',
            controller: 'accepttermsController'
        })

        .state('home.accepttermscomplete', {
            url: '/accepttermscomplete',
            templateUrl: 'home-accepttermscomplete.html',
            controller: 'accepttermscompleteController'
        })

        .state('home.verifyidentity', {
            url: '/verifyidentity',
            templateUrl: 'home-verifyidentity.html',
            controller: 'verifyidentityController'
        })

        .state('home.howitworks', {
            url: '/howitworks',
            templateUrl: 'home-howitworks.html',
            controller: 'howitworksCtrl'
        })

        .state('home.faqs', {
            url: '/faqs',
            templateUrl: 'home-faqs.html'
        })

        .state('home.verification', {
            url: '/verification',
            templateUrl: 'home-verification.html'
        })

        .state('home.paymentoptcomplete', {
            url: '/paymentoptcomplete',
            templateUrl: 'home-paymentoptcomplete.html',
        })

        .state('home.profileoptions', {
            url: '/profileoptions',
            templateUrl: 'home-profileoptions.html',
            controller: 'settingsController'
        })

        .state('home.prosettings', {
            url: '/prosettings',
            templateUrl: 'home-prosettings.html',
            controller: 'prosettingsController'
        })

        .state('home.setrates', {
            url: '/setrates',
            templateUrl: 'home-setrates.html',
            controller: 'setratesController'
        })

        .state('home.stats', {
            url: '/stats',
            templateUrl: 'home-stats.html',
            controller: 'statsController'
        })

        .state('home.recentcalls', {
            url: '/recentcalls',
            templateUrl: 'home-recentcalls.html',
            controller: 'recentCallController'
        })

        .state('home.contacts', {
            url: '/contacts',
            templateUrl: 'home-contacts.html',
            controller: 'contactsController'
        })

        .state('home.blockedusers', {
            url: '/blockedusers',
            templateUrl: 'home-blockedusers.html',
            controller: 'blockedusersController'
        })

        .state('home.editlocation', {
            url: '/editlocation',
            templateUrl: 'home-editlocation.html',
            controller: 'editlocationController'
        })

        .state('home.editaboutme', {
            url: '/editaboutme',
            templateUrl: 'home-editaboutme.html',
            controller: 'editaboutmeController'
        })

        .state('home.search', {
            url: '/search',
            templateUrl: 'home-search.html',
            controller: 'searchController'
        })

        .state('home.updatepayment', {
            url: '/updatepayment',
            templateUrl: 'home-updatepayment.html',
            controller: 'proUpgradeController'
        })

        .state('home.paidphoto', {
            url: '/paidphoto/:media_id/{model_id}/{displayname}',
            templateUrl: 'home-paidphoto.html',
            controller: 'paidphotoController'
        })

        .state('home.sendPhoto', {
            url: '/sendPhoto:recieverId',
            templateUrl: 'home-sendPhoto.html',
            controller: 'sendPhotoController',
            /*resolve:{
                recieverId: ['$stateParams', function($stateParams){
                    return $stateParams.recieverId;
                }]
            }*/
        })

        .state('home.viewprofile', {
            url: '/viewprofile:user_id',
            templateUrl: 'home-viewprofile.html',
            controller: 'viewprofileController'
            /*resolve:{
                user_id: ['$stateParams', function($stateParams){
                    return $stateParams.user_id;
                }]
            }*/
        })

        .state('home.callerhistory', {
            url: '/callerhistory:userId',
            templateUrl: 'home-callerhistory.html',
            controller: 'callerhistoryController'
            /*resolve:{
                userId: ['$stateParams', function($stateParams){
                    return $stateParams.userId;
                }]
            }*/
        })
        
        var rememberme_check = localStorage.getItem("rememberme_flag");
        if( rememberme_check ) {
            $urlRouterProvider.otherwise('/home/affter_login');
        } else {
            $urlRouterProvider.otherwise('/home');
        }

    }).run(['$rootScope', '$state', '$stateParams', function ($rootScope,   $state,   $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $state.transitionTo('home');
    }]);
