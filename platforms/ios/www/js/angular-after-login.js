//+++++++++++++++++++++++++++DashBoard page controller+++++++++++++++++++++

mainApp.controller( "dashBoardController", function( $scope, $http, $timeout, $state, $rootScope, $location  ) {
    $rootScope.previousState;
    $rootScope.currentState;
    $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
        //console.log('Previous state:'+$rootScope.previousState);
        //console.log('Current state:'+$rootScope.currentState);
    });
    $scope.upgradeDisplay = false;
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var username = userData.displayname;
    var user_number = userData.username;
    $scope.proSection = false;
    var getDashboardData = {};
    getDashboardData['responsetype'] = "json";
    getDashboardData['app'] = 1;
    var user_id = JSON.parse(user_detail).data.id;
    //console.log(JSON.stringify(getDashboardData));
    var responsePromise = $http.post(BASE_URL+"dashboard",JSON.stringify(getDashboardData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log( "success:"+JSON.stringify( data ) ); 
        if( data.status != 'error' ) {
            localStorage.setItem("dashboard_data",JSON.stringify(data));
            $('#usersStatus').html(data.data.status);
            $('#user_loc').html(data.data.profile.profile_status);
            $('#profile_pic').attr("src",data.data.profile_image);
            var proDetail = data.data.profile.pro;
            if( proDetail == 0 ) {
                $scope.learn_more = 'Learn more';
                $scope.pro_status = '$$ Earn money with Ringhop Pro.';
            } else if( proDetail == 1 ) {
                $scope.learn_more = 'Complete Ringhop Pro setup';
                $scope.pro_status = 'Ringhop Pro verification process.';
            } else {
                $scope.learn_more = '';
                $scope.pro_status = '';
            }
            
            $scope.proStatusLink = function() {
                if( proDetail == 0 ) {
                    $state.transitionTo('home.learnmore');
                }
                if( proDetail == 1 ) {
                    $state.transitionTo('home.prodashboard');
                } 
            };

            //pro user detail in dashboard
            if( proDetail == 2 ) {
                var pro_number = "("+user_number.substr(0, 3)+") "+user_number.substr(3, 3)+"-"+user_number.substr(6, 4);
                $scope.proSection = true;
                $('#pro_number').html(pro_number);
            }
           // alert(data.data.user_status);
            //console.log("====users status for upgrade option=====");
            if( data.data.user_status == 'pending' ) {
                $scope.upgradeDisplay = true;
            } else {
                $scope.upgradeDisplay = false;
            }
        } else {
            $state.transitionTo('home.login');
        }
        
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( "error:"+JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $('#username').html(username);
    
    $scope.settingLink = function () {
        $state.transitionTo('home.settings');
    };
    $scope.statusAndAvaillink = function () {
        $state.transitionTo('home.phonesettings');
    };
    $scope.chatLink = function () {
        $state.transitionTo('home.messages');
    };
    $scope.proUpgrade = function () {
    	$state.transitionTo('home.upgrade');
    };
    $scope.callLink = function () {
        $state.transitionTo('home.recentcalls');
    };
    $scope.contactsLink = function () {
        $state.transitionTo('home.contacts');
    };
    $scope.searchLink = function () {
        $state.transitionTo('home.search1');
    };
    //console.log('====device part=====');
    var getDeviceData = {};
    getDeviceData['os'] = deviceOS;
    getDeviceData['uuid'] = uuid;
    getDeviceData['version'] = deviceVersion;
    getDeviceData['user_id'] = user_id;
    //console.log(JSON.stringify(getDeviceData));
    var responsePromise = $http.post(BASE_URL+"userphonedetails",JSON.stringify(getDeviceData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log( "success:"+JSON.stringify( data ) ); 
        if( data.status == 'error' ) {
            $state.transitionTo('home');
        }
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( "error:"+JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
} )

//+++++++++++++++++++++++++++Settings page controller+++++++++++++++++++++

mainApp.controller( "settingsController", function( $scope, $http, $timeout, $state ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.user_id = userData.id;
    $scope.editProfileLink = function () {
        $state.transitionTo('home.editProfile');
    };
    $scope.blockingPage = function () {
        $state.transitionTo('home.blockedusers');
    };
    $scope.updateUpgrade = function () {
        $state.transitionTo('home.updatepayment');
    };
    $scope.logout = function ( $event ) {
        var getLogoutData = {};
        getLogoutData['responsetype'] = "json";
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var responsePromise = $http.post(BASE_URL+"logout",JSON.stringify(getLogoutData));
        responsePromise.success( function( data, status, headers, config ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( data.status == 'success' ) {
                localStorage.removeItem("userDetail");
                localStorage.removeItem("rememberme_flag");
                $state.transitionTo('home.login');

            } else {
                console.log('failure');
            }
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            console.log( JSON.stringify( data ) ); 
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.statusAndAvail = function () {
        $state.transitionTo('home.phonesettings');
    };
    $scope.resetPassword = function () {
        $state.transitionTo('home.resetpassword');
    };
    $scope.viewProfile = function () {
        $state.transitionTo('home.viewprofile');
    };
    
} )

//+++++++++++++++++++++++++++editProfile page controller+++++++++++++++++++++

mainApp.controller( "editProfileController", function( $scope, $http, $timeout, $state ) {
    $scope.image_available = false;
    $scope.takePhotoOpt = false;

    $scope.uploadPhoto = function () {
        $scope.takePhotoOpt = true;
    };
    
    $scope.cancelPhoto = function () {
        $scope.takePhotoOpt = false;
    };

    $scope.editProfileLink = function ( $event ) {
        $state.transitionTo('home.editProfile');
    };
    
    function onPhotoDataSuccess(imageData) {
        var profileImage = document.getElementById('profile_image');
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageString = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function ( $event ) {
        $scope.takePhotoOpt = false;
        $scope.image_available = true;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, allowEdit: true, correctOrientation: true,
        destinationType: destinationType.DATA_URL });
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $scope.takePhotoOpt = false;
        $scope.image_available = true;
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: pictureSource.PHOTOLIBRARY });
    };

    // Called if something bad happens.
    function onFail(message) {
        // alert('Failed because: ' + message);
        $scope.image_available = false;
    }

    //upload profile pic
    $scope.saveProfilePic = function ( $event ) {
        var updateProfileData = {};
        updateProfileData['responsetype'] = "json";
        updateProfileData['image'] = imageString;
        //console.log(JSON.stringify(updateProfileData));
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var responsePromise = $http.post(BASE_URL+"uploadresized",JSON.stringify(updateProfileData));
        responsePromise.success( function( data, status, headers, config ) {
            console.log(JSON.stringify(data));
            $('#upload_status').html(data.message);
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $state.transitionTo('home.affter_login');
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };

    //cancel upload
    $scope.cancelUpload = function () {
        $scope.image_available = false;
    };

    $scope.deletePhoto = function () {
        navigator.notification.confirm(
            'Are you sure you want to delete photo?',  // message
            onConfirm,  
            'Delete Photo'
        );
    };

    function onConfirm(button) {
        if( button == 1 ) { 
            var deletePhotoData = {};
            deletePhotoData['responsetype'] = "json";
            console.log( JSON.stringify( deletePhotoData ) );
            var responsePromise = $http.post( BASE_URL+"removeprofilephoto", JSON.stringify( deletePhotoData ) );
            responsePromise.success( function( data, status, headers, config ) {
                console.log( JSON.stringify( data ) );
                $( '#deleteBtn' ).html( data.message );
                $( '#deleteBtn' ).addClass('errorStatus');
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                console.log( JSON.stringify( data ) ); 
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    }

    $scope.setLocation = function() {
        $state.transitionTo( 'home.editlocation' );
    };

    $scope.aboutMe = function() {
        $state.transitionTo( 'home.editaboutme' );
    };

} )

//++++++++++++++++++++++++right menu button+++++++++++++++++++++++++++++++++++

mainApp.controller( "menuController", function( $scope, $http, $timeout, $state, $rootScope ) {
    $scope.leftMenu = function ( $event ) {
        $state.transitionTo('home.settings');
    };
    $scope.backButton = function(){
        console.log('previous=>'+$rootScope.previousState);
        console.log('current=>'+$rootScope.currentState);
        if( $rootScope.currentState == 'home.viewprofile' ){
            if( $rootScope.previousState == 'home.profileoptions' ) {
                console.log('moved');
                $state.transitionTo('home.profileoptions');
            } else {
                console.log('not moved');
                window.history.back();
            }
            
        } else {
            console.log("going in last");
            window.history.back();
        }
    };
} )

//+++++++++++++++++++++++++++phoneSettings page controller+++++++++++++++++++++

mainApp.controller( "availabilityController", function( $scope, $http, $timeout, $state ) {
    var dashboard_data = localStorage.getItem("dashboard_data");
    var statusAvail_data = JSON.parse(dashboard_data).data.status;
    var statusAvail_test = JSON.parse(dashboard_data).data.profile.profile_status;
    //console.log(statusAvail_data);
    if(statusAvail_data == 'Available for Calls Only'){
        $scope.call_available = true;
        $scope.sms_available = false;
    } 
    if(statusAvail_data == 'Available for Texting Only'){
        $scope.call_available = false;
        $scope.sms_available = true;
    } 
    if(statusAvail_data == 'Available for Calls & Texting'){
        $scope.call_available = true;
        $scope.sms_available = true;
    }
    if(statusAvail_test != null){
        $scope.profile_status = statusAvail_test;
    }
    $scope.updateStatus = function ( $event ) {
        var statusData = {};
        statusData['responsetype'] = "json";
        if($scope.sms_available == true){
            statusData['sms_available'] = $scope.sms_available;
        }
        if($scope.call_available == true){
            statusData['call_available'] = $scope.call_available;
        }
        statusData['profile_status'] = $scope.profile_status;
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        //console.log(JSON.stringify(statusData));
        var responsePromise = $http.post(BASE_URL+"phonesettings",JSON.stringify(statusData));
        responsePromise.success( function( data, status, headers, config ) {
            if( data.message == 'user not logged in' ){
                $state.transitionTo('home.login');
            }
            //console.log(JSON.stringify(data));
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $('#availStatusCheck').html(data.message);
        } );
    };
} )

//+++++++++++++++++++++++++++changepwd page controller+++++++++++++++++++++

mainApp.controller( "changepwdController", function( $scope, $http, $timeout, $state ) {
    var flag = false;
    $scope.changePasswd = function( $event ) {
        $('#changepwd_status').html('');
        var password = $scope.password;
        var confirm_passwd = $scope.confirmpassword;
        var oldpassword = $scope.oldpassword;
        var new_val = "";
        var resetFormDiv = document.getElementById('changePasswdForm');
        var input_id = '';
        var elementsLength = parseInt(resetFormDiv.getElementsByTagName('input').length);
        for (var i=0; i<elementsLength; i++) {
            input_id = resetFormDiv.getElementsByTagName('input')[i].id;
            new_val = resetFormDiv.getElementsByTagName('input')[i].value;
            $("label[for='"+input_id+"']").removeAttr('style');
            document.getElementsByTagName("span")[i].innerHTML = '';
            if(new_val == '') {
                document.getElementsByTagName("span")[i].innerHTML = 'This field is required.';
                $("label[for='"+input_id+"']").css('color','red');
            }
        }
        $( '#oldpassword' ).keyup(function (e) {
            if( $('#oldpassword').val().length >= 6)  {
                if( $('#oldpassword').val().length <= 15 ) {  
                    $( '#resetpwdold_error' ).html('');
                    $( '#resetpwdold_label' ).removeAttr('style');
                } else {
                    $( '#resetpwdold_error' ).html('Please enter no more than 15 characters.');
                    $('#resetpwdold_label').css('color','red');
                }
            } else {
                if( $('#oldpassword').val().length == 0)  {
                    $( '#resetpwdold_error' ).html('This field is required.');
                } else {
                    $( '#resetpwdold_error' ).html('Please enter at least 6 characters.');
                    $('#resetpwdold_label').css('color','red');
                }
            }
            if( $('#oldpassword').val().indexOf(' ') >= 0 ) {
                var withoutSpace = $('#oldpassword').val().replace(/ /g,"");
                var withoutSpaceLength = withoutSpace.length;
                if( withoutSpaceLength == 0 ) {
                    $('#resetpwdold_label').css('color','red');
                    $( '#resetpwdold_error' ).html('This field is required.');
                } else if( withoutSpaceLength > 15 ) {
                    $('#resetpwdold_label').css('color','red');
                    $( '#resetpwdold_error' ).html('Please enter no more than 15 characters.');
                } else if( withoutSpaceLength < 6 ) {
                    $('#resetpwdold_label').css('color','red');
                    $( '#resetpwdold_error' ).html('Please enter at least 6 characters.');
                } else {
                    $( '#resetpwdold_error' ).html('');
                    $('#resetpwdold_label').removeAttr('style');
                }
            }
            
        } );
        $( '#new_password' ).keyup(function (e) {
            if( $('#new_password').val().length >= 6)  {
                if( $('#new_password').val().length <= 15 ) {  
                    $( '#resetpwd_error' ).html('');
                    $( '#resetpwd_label' ).removeAttr('style');
                   
                } else {
                    $( '#resetpwd_error' ).html('Please enter no more than 15 characters.');
                    $('#resetpwd_label').css('color','red');
                }
            } else {
                if( $('#new_password').val().length == 0)  {
                    $( '#resetpwd_error' ).html('This field is required.');
                } else {
                    $( '#resetpwd_error' ).html('Please enter at least 6 characters.');
                    $( '#resetpwd_label' ).css('color','red');
                }
            }
            if( $('#new_password').val().indexOf(' ') >= 0 ) {
                var withoutSpace = $('#new_password').val().replace(/ /g,"");
                var withoutSpaceLength = withoutSpace.length;
                if( withoutSpaceLength == 0 ) {
                    $( '#resetpwd_error' ).html('This field is required.');
                    $('#resetpwd_label').css('color','red');
                } else if( withoutSpaceLength > 15 ) {
                    $( '#resetpwd_error' ).html('Please enter no more than 15 characters.');
                    $('#resetpwd_label').css('color','red');
                } else if( withoutSpaceLength < 6 ) {
                    $( '#resetpwd_error' ).html('Please enter at least 6 characters.');
                    $('#resetpwd_label').css('color','red');
                } else {
                    $( '#resetpwd_error' ).html('');
                    $( '#resetpwd_label' ).removeAttr('style');
                }
            }
            
        } );
        $( '#con_resetpassword' ).keyup(function (e) {
            if( $('#con_resetpassword').val().length >= 6)  {
                if( $('#con_resetpassword').val().length <= 15 ) {  
                    $( '#resetpwdcon_error' ).html('');
                    $( '#resetpwdcon_label' ).removeAttr('style');
                } else {
                    $( '#resetpwdcon_error' ).html('Please enter no more than 15 characters.');
                    $('#resetpwdcon_label').css('color','red');
                }
            } else {
                if( $('#con_resetpassword').val().length == 0)  {
                    $( '#resetpwdcon_error' ).html('This field is required.');
                } else {
                    $( '#resetpwdcon_error' ).html('Please enter at least 6 characters.');
                    $('#resetpwdcon_label').css('color','red');
                }
            }
            if( $('#con_resetpassword').val().indexOf(' ') >= 0 ) {
                var withoutSpace = $('#con_resetpassword').val().replace(/ /g,"");
                var withoutSpaceLength = withoutSpace.length;
                if( withoutSpaceLength == 0 ) {
                    $( '#resetpwdcon_error' ).html('This field is required.');
                    $('#resetpwdcon_label').css('color','red');
                } else if( withoutSpaceLength > 15 ) {
                    $( '#resetpwdcon_error' ).html('Please enter no more than 15 characters.');
                    $('#resetpwdcon_label').css('color','red');
                } else if( withoutSpaceLength < 6 ) {
                    $( '#resetpwdcon_error' ).html('Please enter at least 6 characters.');
                    $('#resetpwdcon_label').css('color','red');
                } else {
                    $( '#resetpwdcon_error' ).html('');
                    $( '#resetpwdcon_label' ).removeAttr('style');
                }
            }
        } );

        if ( $('#oldpassword').val() != '' ) {
            var withoutSpace = $('#oldpassword').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#resetpwdold_error' ).html('This field is required.');
                $( '#resetpwdold_label' ).css('color','red');
            } else if( withoutSpaceLength > 15 ) {
                $( '#resetpwdold_error' ).html('Please enter no more than 15 characters.');
                $( '#resetpwdold_label' ).css('color','red');
            } else if( withoutSpaceLength < 6 ) {
                $( '#resetpwdold_error' ).html('Please enter at least 6 characters.');
                $( '#resetpwdold_label' ).css('color','red');
            } else {
                $( '#resetpwdold_error' ).html('');
                $( '#resetpwdold_label' ).removeAttr('style');
            }
        }

        if ( $('#new_password').val() != '' ) {
            var withoutSpace = $('#new_password').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#resetpwd_label' ).css('color','red');
                $( '#resetpwd_error' ).html('This field is required.');
            } else if( withoutSpaceLength > 15 ) {
                $( '#resetpwd_label' ).css('color','red');
                $( '#resetpwd_error' ).html('Please enter no more than 15 characters.');
            } else if( withoutSpaceLength < 6 ) {
                $( '#resetpwd_label' ).css('color','red');
                $( '#resetpwd_error' ).html('Please enter at least 6 characters.');
            } else {
                $( '#resetpwd_label' ).removeAttr('style');
                $( '#resetpwd_error' ).html('');
            }
        }

        if( $('#con_resetpassword').val() != '' ) {
            var withoutSpace = $('#con_resetpassword').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#resetpwdcon_error' ).html('This field is required.');
                $( '#resetpwdcon_label' ).css('color','red');
            } else if( withoutSpaceLength > 15 ) {
                $( '#resetpwdcon_error' ).html('Please enter no more than 15 characters.');
                $( '#resetpwdcon_label' ).css('color','red');
            } else if( withoutSpaceLength < 6 ) {
                $( '#resetpwdcon_error' ).html('Please enter at least 6 characters.');
                $( '#resetpwdcon_label' ).css('color','red');
            } else {
                $( '#resetpwdcon_error' ).html('');
                $( '#resetpwdcon_label' ).removeAttr('style');
            }
        } 

        if( $('#resetpwdold_error').html() == '' && $('#resetpwd_error').html() == '' && $('#resetpwdcon_error').html() == ''){
            flag = true;
        }
        if( flag == true ) {
            if(password == confirm_passwd) {
                var getResetData = {};
                getResetData['oldpassword'] = oldpassword;
                getResetData['password'] = password;
                getResetData['confirmpassword'] = confirm_passwd;
                getResetData['responsetype'] = 'json';
                console.log( JSON.stringify( getResetData ) );
                $('.ui-loader').show();
                var responsePromise = $http.post( BASE_URL+"changepassword",JSON.stringify(getResetData));
                responsePromise.success( function( data, status, headers, config ) {
                    $('.ui-loader').hide();
                    console.log(JSON.stringify(data));
                    $('#changepwd_status').html( data.message );
                    
                } );
                responsePromise.error( function ( data, status, headers, config ) {
                    $('.ui-loader').hide();
                    console.log( JSON.stringify( data ) ); 
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            } else {
                $('#changepwd_status').html( 'Password and confirm password mismatch' );
            }
        }
    };
} )


//+++++++++++++++++++++++++++TextMessage page controller+++++++++++++++++++++

mainApp.controller( "recentMessageController", function( $scope, $http, $state ) {
    $scope.prevDisabled = false;
    $scope.nextDisabled = false;
    $scope.messageOptions = false;
    $scope.paginateSection = false;
    $scope.noMessageDiv = false;
    $scope.limit = 2;
    var pre_count = 0;
    var next_count = 0;
    //var chatArr;
    var users = 'billable';
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    if( proData == 2 ) {
        //for pro users
        $scope.messageOptions = true;
    }
    var getMessageData1 = {};
    getMessageData1['responsetype'] = 'json';
    //console.log( JSON.stringify( getMessageData1 ) );
    var responsePromise = $http.post( BASE_URL+"messages",JSON.stringify( getMessageData1 ) );
    responsePromise.success( function( data, status, headers, config ) {
        //console.log( JSON.stringify( data ) );
        if( data.message == 'user not logged in' ){
            //if user's session expires
            $state.transitionTo('home.login');
        }
        $scope.limit = data.data.per_page; //update the max limit of message per page
        $scope.recentChats = data.data.msghistory;
        $scope.totalMessage = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            //alert($scope.totalMessage);
            if( $scope.totalMessage == 0 ){
                $scope.noMessageDiv = true;
                $scope.noMessage = "No recent messages available.";
            }
            
        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        $('.phpdebugbar-openhandler-overlay').hide();
        $('.ui-loader').hide();
    } );
    $scope.billableShow = true;
    $scope.billableUser = function () {
        pre_count = 0;
        next_count = 0;
        users = 'billable';
        $('#billable_user').addClass('fullwidth_blue');
        $('#billable_user').removeClass('fullwidth_white');
        $('#trial_user').addClass('fullwidth_white');
        $('#trial_user').removeClass('fullwidth_blue');

        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var getMessageData1 = {};
        getMessageData1['responsetype'] = 'json';
        //console.log(JSON.stringify(getMessageData1));
        var responsePromise = $http.post(BASE_URL+"messages",JSON.stringify(getMessageData1));
        responsePromise.success( function( data, status, headers, config ) {
            //console.log(JSON.stringify(data));
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.limit = data.data.per_page;
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
           // alert('server error');
        } );
    };
    $scope.trialUser = function(){
        pre_count = 0;
        next_count = 0;
        users = 'trial';
        $('#billable_user').addClass('fullwidth_white');
        $('#billable_user').addClass('fullwidth_blue');
        $('#trial_user').addClass('fullwidth_blue');
        $('#trial_user').removeClass('fullwidth_white');
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var getMessageData = {};
        getMessageData['responsetype'] = 'json';
        getMessageData['sort'] = 'free';
        //console.log( JSON.stringify( getMessageData ) );
        var responsePromise = $http.post( BASE_URL+"messages", JSON.stringify( getMessageData ) );
        responsePromise.success( function( data, status, headers, config ) {
            //console.log(JSON.stringify(data));
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.limit = data.data.per_page;
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
            
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
            //alert('server error');
        } );
    };
    $scope.replyLink = function () {
        $state.transitionTo('home.reply');
    };
    $scope.prevBtn = function () {
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.prevDisabled = true;
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getMessageData1['page'] = pre_count;
            if ( users == 'trial' ) {
                getMessageData1['sort'] = 'free';
            }
            var responsePromise = $http.post( BASE_URL+"messages", JSON.stringify( getMessageData1 ) );
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.recentChats = data.data.msghistory;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
                next_count = next_count - 1;
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                console.log( JSON.stringify( data ) ); 
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
        
    };
    $scope.nextBtn = function () {
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            console.log('more messages are to load..');
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getMessageData1['page'] = next_count+1;
            if ( users == 'trial' ) {
                getMessageData1['sort'] = 'free';
            }
            //console.log( JSON.stringify( getMessageData1 ) );
            var responsePromise = $http.post( BASE_URL+"messages", JSON.stringify( getMessageData1 ) );
            responsePromise.success( function( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                //console.log(JSON.stringify(data));
                $scope.limit = data.data.per_page;
                $scope.recentChats = data.data.msghistory;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.nextDisabled = true;
                }
                
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };
    
} )

//+++++++++++++++++++++++++++Reply page controller+++++++++++++++++++++

mainApp.controller( "replyController", function( $scope, $http, $state, $stateParams ) {
    var subscribeCount = 0;
    $('.phpdebugbar-openhandler-overlay').hide();
    loadCount = loadCount+1;
    //alert("loadCount=>"+loadCount);
    $scope.popupShow = false;
    $scope.imgPopupShow = false;
    $scope.closeMsgPopup = function () {
        $(".popup1").addClass('ng-hide');
        $scope.imgPopupShow = false;
        $('#overlay').hide();
    };
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var userId = userData.id;
    var originator_id = $stateParams.messageId;
    $scope.user_id = originator_id;
    var display_name = '';
    var channel1;
    var getReplyData = {};
    getReplyData['responsetype'] = 'json';
    getReplyData['uid'] = originator_id;
    getReplyData['displayname'] = display_name;
    //console.log(JSON.stringify(getReplyData));
    var responsePromise = $http.post(BASE_URL+"reply",JSON.stringify(getReplyData));
    responsePromise.success( function( data, status, headers, config ) {
        localStorage.setItem("reply_data",JSON.stringify(data));
        if( data.message == 'user not logged in' ) {
            $state.transitionTo('home.login');
        }
        console.log(JSON.stringify(data));
        $scope.replyChats = data.data;
        $scope.username = data.data.originator.displayname;
        //alert($scope.username);
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    if( parseInt( userId ) < parseInt( originator_id ) ) {
    	channel1 = userId+'X'+originator_id;
    } else {
    	channel1 = originator_id+'X'+userId;
    }
    console.log(channel1);
    $.cloudinary.config({ cloud_name: 'nobetek-llc', api_key: '247749274532722'});
    var pubnub = PUBNUB.init({
        publish_key: 'pub-c-d69a065d-1b7c-4619-baf8-f240601221bd',
        subscribe_key: 'sub-c-f7bbad58-bf8d-11e3-a219-02ee2ddab7fe',
        auth_key: userId,
        uuid: userId,
        ssl: true
    });
    if( loadCount%2 == 1 ) {
        //alert('odd numbr called');
        pubnub.state({
            channel  : channel1,
            uuid     : userId,
            callback : function(m){
                console.log('in pubnub state:'+m);
                console.log('histry called');
                pubnub.history({
                channel: channel1,
                count: 10,
                end: m.lastdelete,
                callback: function(history){
                    history = history[0];
                    //message = message || [];
                    console.log("History");
                    //console.log(history);
                    
                    var user_session = userId;

                    for(var i = 0; i < history.length; i++) {
                        
                        if (history[i].blocked == 'yes') {
                            //just a simple way to not display messages that users sent while in a blocked status
                        } else if (history[i].msg_type == 'video_sale') {
                            //check if a photo purchase request sent and display accordingly
                            if (user_session == history[i].originator_u_id) {
                            } else {
                                //$("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#424242;color:white;"><div><div style="font-size:12px;"><i>User has sent a paid photo request.</i></div><div style="margin-top:10px"><a href="txt_message/photo_purchase.php?rdisplay=<?php echo $receiver_display; ?>&media_id=' + history[i].media_id  +  '&model_id=' +  history[i].originator_u_id +  '"><img src="img/loving_it.png" height="40" width="40" border="0" alt="loving it" /></a><a href="txt_message/photo_purchase.php?rdisplay=<?php echo $receiver_display; ?>&media_id=' + history[i].media_id  +  '&model_id=' +  history[i].originator_u_id +  '" data-ajax="false" style="color:#7ebbea;">Click to view photo</a></div></div></div>');
                                $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#424242;color:white;"> <div> <div style="font-size:12px;"> <i>User has sent a paid photo request.</i> </div> <div style="margin-top:10px"> <a href="#/home/paidphoto/'+history[i].media_id+'/'+history[i].originator_u_id+'/'+$scope.username+'"> <img src="img/loving_it.png" height="40" width="40" border="0" alt="loving it" /> </a> <a href="#/home/paidphoto/'+history[i].media_id+'/'+history[i].originator_u_id+'/'+$scope.username+'" data-ajax="false" style="color:#7ebbea; text-decoration:underline; "> Click to view photo </a> </div> </div> </div>');
                            }
                        } else if (history[i].msg_type == 'trialexpired' || history[i].msg_type == 'upgrade') {
                            //just a simple way to not display messages that users sent while in a trial expired status
                        }
            
                        // start image insertion
                        else if (typeof history[i].cloudinary_photo_id != 'undefined' && history[i].cloudinary_photo_id) {
                            console.log("history cloudinary..");
                            var large_image_url = $.cloudinary.url(history[i].cloudinary_photo_id, 
                                { 
                                    width: 300, height: 460,
                                    crop: 'fit'
                                });
                            var small_image_url = $.cloudinary.image(history[i].cloudinary_photo_id, 
                                { 
                                    width: 100, height: 150, 
                                    crop: 'fit',fetch_format: 'png',
                                    radius: 15
                                });
                            var large_splitted_url = large_image_url.split('//');
                            var small_splitted_url = $(small_image_url).attr("src").split('//');
                            var li = '';
                            if ( history[i].originator_u_id !=  user_session ) {
                                li = $("<div class='you' style='white-space:normal; padding: .7em 1em;'> </div>");
                            } else {
                                li = $("<div class='me' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'> </div>");
                            }
                            var link = $('<a href="javascript:;" id="message_image" data="http://' + large_splitted_url[1] + '"></a>').append('<img src="http://'+small_splitted_url[1]+'">');    
                            $(li).append(link);
                            $("#msg_container").append(li);

                        }
                            
                        //check if message from history is for this user or sender and prepare html
                        else if ( history[i].originator_u_id !=  user_session ) {
                            $("#msg_container").append("<div class='you'> <p>"+history[i].user_msg+"</p> <span>"+ history[i].date_time +"</span></div>");
                        } else {
                            $("#msg_container").append("<div class='me'> <p>"+history[i].user_msg+"</p> <span>"+ history[i].date_time +"</span></div>");
                        }

                    
                        $('html, body').scrollTop( $(document).height() );
                    }
                }
            });

            //end of new insert
            },
            error: function(m){
                console.log(m)
            }

        });
        pubnub.subscribe({
            channel  : channel1 ,
            callback : function(message) {
                subscribeArr = [];
                subscribeCount = subscribeCount+1;
                subscribeArr.push(subscribeCount);
                console.log("subscribeCount=>"+subscribeCount);
                console.log("subscribe array=>"+subscribeArr);
                console.log('message is:'+message.user_msg + ' message blocker is' + message.blocker +'blocked:'+message.blocked+' originator_u_id:'+message.originator_u_id);
                console.log('userId: '+userId);   
                // if( subscribeCount == 1 ){
                    var user_session = userId;
                    if ( message.blocked == 'yes' ) {
                        console.log('user blocked..');
                        if (user_session == message.originator_u_id ) {
                            if (user_session == message.blocker){
                                $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#ff9600;color:white;'> <p>"+'You are attempting to send a message to a user you have blocked. If you do wish to send messages to this user, please first remove from block list.'+"</p> <span>"+ message.date_time +"</span></div>");
                            } else {
                                $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>"+'This user no longer wishes to receive your messages :('+"</p> <span>"+ message.date_time +"</span></div>");
                            }
                        }   
                    } else if (message.msg_type == 'video_sale') {
                        //check if a photo purchase request sent and display accordingly
                        if (user_session == message.originator_u_id) {
                        } else {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#424242;color:white;"> <div> <div style="font-size:12px;"> <i>User has sent a paid photo request.</i> </div> <div style="margin-top:10px"> <a href="#/home/paidphoto/'+message.media_id+'/'+message.originator_u_id+'/'+$scope.username+'"> <img src="img/loving_it.png" height="40" width="40" border="0" alt="loving it" /> </a> <a href="#/home/paidphoto/'+message.media_id+'/'+message.originator_u_id+'/'+$scope.username+'" data-ajax="false" style="color:#7ebbea;text-decoration:underline;"> Click to view photo </a> </div> </div> </div>');
                        }
                    } else if (message.msg_type == 'trialexpired' || message.msg_type == 'upgrade') {
                        if (user_session == message.originator_u_id) {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">Your free trial has expired and you must upgrade to continue chatting. <p></p><a href="../upgrade/" data-ajax="false">Upgrade now</a></div>');
                        } else {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">This user has run out of trial msgs and must upgrade to continue.</div>');
                        }
                    } else if ( message.txt_available ==  0 && user_session == message.originator_u_id) {
                        console.log('not available..');
                        $("#msg_container").append("<div class='me'> <p>"+'Zzzz. I am not currently available for texting, but I got your message. ttyl.'+"</p> <span>"+ message.date_time +"</span></div>");
                    }
                    // start image insertion
                    else if (typeof message.cloudinary_photo_id != 'undefined' && message.cloudinary_photo_id) {
                        console.log('cloudinary..');
                        var large_image_url = $.cloudinary.url(message.cloudinary_photo_id, 
                            { 
                                width: 300, height: 460,
                                crop: 'fit'
                            });
                        var small_image_url = $.cloudinary.image(message.cloudinary_photo_id, 
                            { 
                                width: 100, height: 150, 
                                crop: 'fit',fetch_format: 'png',
                                radius: 15
                            });
                        var large_splitted_url = large_image_url.split('//');
                        var small_splitted_url = $(small_image_url).attr("src").split('//');
                        var li = '';
                        if ( message.originator_u_id !=  user_session ) {
                            li = $("<div class='me'> </div>");
                        } else {
                            li = $("<div class='you'> </div>");
                        }
                        var link = $('<a href="javascript:;" id="message_image" data="http://' + large_splitted_url[1] + '"></a>').append('<img src="http://'+small_splitted_url[1]+'">');    
                        $(li).append(link);
                        $("#msg_container").append(li);
                    }
                    //check if message from history is for this user or sender and prepare html
                    else if ( message.originator_u_id !=  user_session ) {
                        console.log('msg appending..');
                        $("#msg_container").append("<div class='you'> <p>"+message.user_msg+"</p> <span>"+ message.date_time +"</span></div>");
                    } else {
                        console.log('appending again..');
                        $("#msg_container").append("<div class='me'> <p>"+message.user_msg+"</p> <span>"+ message.date_time +"</span></div>");
                        var pro_info = '';
                        if ( message.pro == 2 ) {
                            //fc7ea1    
                            if (message.sender_user_status != 'active') {
                                pro_info = message.sender_user_status ;
                            }
                        }
                    }
                    $('html, body').scrollTop( $(document).height() );
                    $('#chatAudio')[0].play();
                // }
            }
        });

        $('#msg_container').on('click', '#message_image', function(event) {
            var bigimageurl = $(this).attr( "data" );
            $("#bigMsgImg").attr("src",bigimageurl); 
            $(".popup1").removeClass('ng-hide');
            $scope.imgPopupShow = true;
            $('#overlay').show();
        });

        $scope.popupLink = function() {
        	//alert('popup clicked ');
            $scope.popupShow = true;
            $('#overlay').show();
            $('#BMPopup').show();
        };

    } 
    
    $scope.submitChat = function($event){
    	//alert("submit message");
        var inputChat = $scope.chatInput;
        if( $('#input_chat').val() != '' ) {
            var getChatData = {};
            getChatData['chat_entry'] = inputChat;
            getChatData['receiver'] = originator_id;
            getChatData['responsetype'] = 'json';
            //console.log( JSON.stringify( getChatData ) );
            var responsePromise = $http.post( BASE_URL+"sendchatmessage",JSON.stringify( getChatData ) );
            responsePromise.success( function( data, status, headers, config ) {
                //console.log(JSON.stringify(data));
                localStorage.setItem("reply_data",JSON.stringify(data));
                $scope.chatInput = '';
                $('html, body').scrollTop( $(document).height() );
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                console.log( JSON.stringify( data ) ); 
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
        $event.preventDefault();
    };

    $scope.deleteHistory = function(){
        console.log('deleting..');
        var pubnub = PUBNUB.init({
            publish_key : 'pub-c-d69a065d-1b7c-4619-baf8-f240601221bd',
            subscribe_key : 'sub-c-f7bbad58-bf8d-11e3-a219-02ee2ddab7fe',
            auth_key : userId,
            uuid : userId
        });

        pubnub.time(function(time) {

            pubnub.state({
                channel : channel1,
                state : {
                    'lastdelete' : time
                },
                callback : function(m) {
                    console.log('deleted..');
                    $("#msg_container").empty();

                },
                error : function(m) {
                    console.log(m)
                }
            });

        });
    };
    //popup link

    $('#overlay').click(function(){
        $('#overlay').hide();
        $('#BMPopup').hide();
        $(".popup1").addClass('ng-hide');
        $scope.imgPopupShow = false;
    });

    $scope.blockUser = function(){
        navigator.notification.confirm(
            'Are you sure you want to block this user?',  // message
            onConfirm,  
            'Block User'
        );
    };
    function onConfirm(button) {
        if( button == 1 ) { 
            var blockcontactData = {};
            blockcontactData['responsetype'] = 'json';
            blockcontactData['caller_id'] = originator_id;
            //console.log( JSON.stringify( blockcontactData ) );
            var responsePromise = $http.post(BASE_URL+"blockuser",JSON.stringify(blockcontactData));
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( JSON.stringify( data ) );
                console.log('blocked');
                $('#user_block').html('<span class="glyphicon glyphicon-ban-circle"></span>already blocked');
                $('#user_block').addClass('blockedMessage');
            } );
        }
    }
    //alert('test at last');

} )

//+++++++++++++++++++++++++++paidphoto page controller+++++++++++++++++++++

mainApp.controller( "paidphotoController", function( $scope, $http, $state, $stateParams ) {
    var paidPhotoData = {};
    paidPhotoData['displayname'] = $stateParams.displayname;
    paidPhotoData['media_id'] = $stateParams.media_id;
    paidPhotoData['model_id'] = $stateParams.model_id;
    paidPhotoData['responsetype'] = 'json';
    //console.log( 'photo data'+JSON.stringify( paidPhotoData ) ); 
    var responsePromise = $http.post(BASE_URL+"photopurchase",JSON.stringify(paidPhotoData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log( 'success'+JSON.stringify( data ) ); 
        $scope.photoInfo = data;
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( 'error'+JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.purchasePhoto = function () {
        if( $scope.photoInfo.button_color == 'red' ) {
            $state.transitionTo('home.reply',{ messageId:$scope.photoInfo.userid });
        } else if ( $scope.photoInfo.button_color == 'green' ) {
            var purchasePhotoData = {};
            purchasePhotoData['media_id'] = $stateParams.media_id;
            purchasePhotoData['displayname'] = $stateParams.displayname;
            purchasePhotoData['model_id'] = $stateParams.model_id;
            purchasePhotoData['responsetype'] = 'json';
            //console.log( 'purchase data'+JSON.stringify( purchasePhotoData ) ); 
            var responsePromise = $http.post(BASE_URL+"processpurchase",JSON.stringify(purchasePhotoData));
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( 'success'+JSON.stringify( data ) ); 
                $state.transitionTo('home.reply',{ messageId:$scope.photoInfo.userid });
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                console.log( 'error'+JSON.stringify( data ) ); 
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
} )

//+++++++++++++++++++++++++++sendPhoto page controller+++++++++++++++++++++

mainApp.controller( "sendPhotoController", function( $scope, $http, $state, $stateParams ) {
    $scope.takePhotoOpt = false;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.pro_data = false;
    if( proData == 2 ) {
        //for pro users
        $scope.pro_data = true;
    }
    $scope.div_visible = true;

    $scope.uploadChatPhoto = function () {
        $scope.takePhotoOpt = true;
    };
    
    $scope.cancelPhoto = function () {
        $scope.takePhotoOpt = false;
    };

    function onPhotoDataSuccess( imageData ) {
        var profileImage = document.getElementById( 'chat_image' );
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageString = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function ( $event ) {
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, allowEdit: true, correctOrientation: true,
        destinationType: destinationType.DATA_URL });
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: pictureSource.PHOTOLIBRARY });
    };

    // Called if something bad happens.
    function onFail(message) {
        // alert('Failed because: ' + message);
        $scope.takePhotoOpt = false;
        $scope.div_visible = true;
    }

    //upload profile pic
    $scope.saveChatPic = function ( $event ) {
        var chatImageData = {};
        var mediaCharge = $('#media_price').val();
        chatImageData['responsetype'] = "json";
        chatImageData['image'] = imageString;
        chatImageData['uid'] = $stateParams.recieverId;
        if( proData == 2 ) {
            if( mediaCharge == '' ) {
                chatImageData['media_price'] = 0;
            } else {
                chatImageData['media_price'] = mediaCharge;
            }
        } else {
            chatImageData['media_price'] = 0;
        }
        //console.log(JSON.stringify(chatImageData));
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var responsePromise = $http.post(BASE_URL+"uploadchat_media",JSON.stringify(chatImageData));
        responsePromise.success( function( data, status, headers, config ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            //console.log('success'+JSON.stringify(data));
            $('#uploadChat_id').html('Photo message sent!');
            $('#uploadChat_id').css('color','green');
            $('#media_price').hide();
            $('#sendBtn').hide();
            $('#cancelBtn').hide();
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( 'error'+JSON.stringify( data ) ); 
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
            //alert('server error');
        } );
    };

    $scope.cancelUpload = function () {
        $scope.div_visible = true;
    };

} )

//+++++++++++++++++++++++++++Recent call page controller+++++++++++++++++++++

mainApp.controller( "recentCallController", function( $scope, $http, $state, $stateParams ) {
    $scope.prevDisabled = false;
    $scope.nextDisabled = false;
    $scope.paginateSection = false;
    $scope.noCallDiv = false;
    $scope.limit = 2;
    var pre_count = 0;
    var next_count = 0;
    var nextCount = 0;
    var getCallData = {};
    getCallData['responsetype'] = 'json';
    //console.log( JSON.stringify( getCallData ) );
    var responsePromise = $http.post(BASE_URL+"recentcalls",JSON.stringify(getCallData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log(JSON.stringify(data));
        if( data.message == 'user not logged in' ){
            $state.transitionTo('home.login');
        }
        $scope.limit = data.data.per_page;
        $scope.recentCalls = data.data.callhistory;
        $scope.totalCall = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;
        if ( $scope.totalCall <= $scope.currentCount ) {
            $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalCall == 0 ){
                $scope.noCallDiv = true;
                $scope.noCall = "No recent calls available.";
            }
        }  else {
            $scope.paginateSection = true;
            $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        $('.phpdebugbar-openhandler-overlay').hide();
        $('.ui-loader').hide();
       // alert('server error');
    } );
    $scope.prevBtn = function () {
        pre_count = next_count;
        if ( $scope.currentPage == 1 ) {
            $('#prevBtnCall').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.prevDisabled = true;
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getCallData['page'] = pre_count;
            //console.log( JSON.stringify( getCallData ) );
            var responsePromise = $http.post( BASE_URL+"recentcalls", JSON.stringify( getCallData ) );
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentCalls = data.data.callhistory;
                $scope.totalCall = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    $('#prevBtnCall').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                } else {
                    $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtnCall').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
                next_count = next_count - 1;
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                console.log( JSON.stringify( data ) ); 
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.nextBtn = function () {
        next_count = next_count + 1;
        if ( $scope.totalCall >= $scope.limit && $scope.currentCount < $scope.totalCall ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getCallData['page'] = next_count+1;
            //console.log( JSON.stringify( getCallData ) );
            var responsePromise = $http.post( BASE_URL+"recentcalls", JSON.stringify( getCallData ) );
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentCalls = data.data.callhistory;
                $scope.totalCall = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalCall > $scope.currentCount ) {
                    $('#nextBtnCall').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.nextDisabled = true;
                }
            } );
        } else {
            $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };
} )

//+++++++++++++++++++++++++++Caller History page controller+++++++++++++++++++++

mainApp.controller( "callerhistoryController", function( $scope, $http, $state, $stateParams ) {
    var getCallerHistoryData = {};
    getCallerHistoryData['responsetype'] = 'json';
    getCallerHistoryData['uid'] = $stateParams.userId;
    //console.log(JSON.stringify(getCallerHistoryData));
    var responsePromise = $http.post(BASE_URL+"callerhistory",JSON.stringify(getCallerHistoryData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log(JSON.stringify(data));
        if( data.message == 'user not logged in' ){
            $state.transitionTo('home.login');
        }
        $scope.callerHistory = data.data;
        if( data.data.isblocked == true ) {
            $('#alreadyBlocked').removeClass('redCallHistory');
            $('#alreadyBlocked').addClass('italicCall');
            $('#alreadyBlocked').html('already blocked');
        } else {
            $('#alreadyBlocked').removeClass('italicCall');
            $('#alreadyBlocked').addClass('redCallHistory');
            $('#alreadyBlocked').html('Block this user');
            $scope.blockUser = function(){
                if($('#alreadyBlocked').html()=='Block this user') {
                    navigator.notification.confirm(
                        'Are you sure you want to block this user?',  // message
                        onConfirm,  
                        'Block User'
                    );
                }
            };
        }
        function onConfirm(button) {
            if( button == 1 ) { 
                var blockUsrData = {};
                blockUsrData['responsetype'] = 'json';
                blockUsrData['caller_id'] = data.data.caller.id;
                var responsePromise = $http.post(BASE_URL+"blockuser",JSON.stringify(blockUsrData));
                responsePromise.success( function( data, status, headers, config ) {
                    $('#alreadyBlocked').html('user blocked');
                    $('#alreadyBlocked').removeClass('redCallHistory');
                    $('#alreadyBlocked').addClass('italicCall');
                } );
            }
        }
        if( data.data.iscontact == true ) {
            $('#alreadyContact').removeClass('blueCallHistory');
            $('#alreadyContact').addClass('italicCall');
            $('#alreadyContact').html('already a contact');
        } else {
            $('#alreadyContact').removeClass('italicCall');
            $('#alreadyContact').addClass('blueCallHistory');
            $('#alreadyContact').html('Add as contact');
            $scope.addAsContact = function(){
                var addContactData = {};
                addContactData['responsetype'] = 'json';
                addContactData['caller_id'] = data.data.caller.id;
                var responsePromise = $http.post(BASE_URL+"addcontact",JSON.stringify(addContactData));
                responsePromise.success( function( data, status, headers, config ) {
                    $('#alreadyContact').html('contact added');
                    $('#alreadyContact').removeClass('blueCallHistory');
                    $('#alreadyContact').addClass('italicCall');
                } );
            };
        }
        
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    //profile page link from profile pic
    $scope.profileLink = function() {
        $state.transitionTo('home.profile');
    };

} )

//+++++++++++++++++++++++++++Contacts page controller+++++++++++++++++++++

mainApp.controller( "contactsController", function( $scope, $http, $state, $stateParams ) {
    var getContactsData = {};
    $scope.noContactDiv = false;
    getContactsData['responsetype'] = 'json';
    var responsePromise = $http.post(BASE_URL+"contacts",JSON.stringify(getContactsData));
    responsePromise.success( function( data, status, headers, config ) {
        console.log(JSON.stringify(data));
        //alert(data.data.length);
        if(data.data.length == 0){
            $scope.noContactDiv = true;
            $scope.noContacts = "No contacts available.";
            alert($scope.noContacts);
        }
        if( data.message == 'user not logged in' ){
            $state.transitionTo('home.login');
        }
        $scope.contacts = data.data;
        $scope.openPopup = function(contactId){
            $('#'+contactId).show();
            $('#overlay').show();
        };
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    
    $scope.deleteContact = function(contactId,id1){
        navigator.notification.confirm(
            'Are you sure you want to delete this contact?',  // message
            onConfirm,  
            'Delete Contact'
        );
        function onConfirm(button){
            if( button == 1 ) {
                var deleteContactData = {};
                deleteContactData['responsetype'] = 'json';
                deleteContactData['contact_id'] = contactId;
                //console.log(JSON.stringify(deleteContactData));
                var responsePromise = $http.post(BASE_URL+"removecontact",JSON.stringify(deleteContactData));
                responsePromise.success( function( data, status, headers, config ) {
                    //console.log(JSON.stringify(data));
                    $('#'+id1).hide();
                    $('#overlay').hide();
                    $('#contact_'+id1).hide("medium", function(){ $(this).remove(); });
                } );
                responsePromise.error( function ( data, status, headers, config ) {
                    console.log( JSON.stringify( data ) ); 
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            }
        }
    };
    $scope.cancelPopup = function(contactId){
        $('#'+contactId).hide();
        $('#overlay').hide();
    };

} )

//+++++++++++++++++++++++++++blockedusers page controller+++++++++++++++++++++

mainApp.controller( "blockedusersController", function( $scope, $http, $state, $stateParams ) {
    var getBlockUserData = {};
    getBlockUserData['responsetype'] = 'json';
    //console.log( JSON.stringify( getBlockUserData ) ); 
    var responsePromise = $http.post(BASE_URL+"blockedusers",JSON.stringify(getBlockUserData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log( JSON.stringify( data ) ); 
        if( data.message == 'user not logged in' ){
            $state.transitionTo('home.login');
        }
        $scope.blockUsers = data.data;
        $scope.openPopup = function(blockId){
            $('#'+blockId).show();
            $('#overlay').show();
        };
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    
    $scope.unblockUser = function(blockId,id1){
        navigator.notification.confirm(
            'Are you sure you want to UN-BLOCK this contact?',  // message
            onConfirm,  
            'UN-BLOCK Contact'
        );
        function onConfirm( button ) {
            if( button == 1 ) {
                var unblockData = {};
                unblockData['responsetype'] = 'json';
                unblockData['contact_id'] = blockId;
                //console.log(JSON.stringify(unblockData));
                var responsePromise = $http.post(BASE_URL+"unblock",JSON.stringify(unblockData));
                responsePromise.success( function( data, status, headers, config ) {
                    //console.log(JSON.stringify(data));
                    $('#'+id1).hide();
                    $('#overlay').hide();
                    $('#blockUser_'+id1).hide("medium", function(){ $(this).remove(); });
                } );
                responsePromise.error( function ( data, status, headers, config ) {
                    console.log( JSON.stringify( data ) ); 
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            }
        }
    };
    $scope.cancelPopup = function(blockId){
        $('#'+blockId).hide();
        $('#overlay').hide();
    };

} )

//+++++++++++++++++++++++++++editlocation page controller+++++++++++++++++++++

mainApp.controller( "editlocationController", function( $scope, $http, $state, $stateParams ) {
    var flag = false;
    var locationSetData = {};
    locationSetData['responsetype'] = 'json';
    //console.log(JSON.stringify(locationSetData));
    var responsePromise = $http.post(BASE_URL+"editlocation",JSON.stringify(locationSetData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log(JSON.stringify(data));
        $( '#userLocation' ).val(data.data.location);
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log(JSON.stringify(data));
    } );
    $scope.updateLocation = function() {
        $('#locationStatusCheck').html('');
        if( $('#userLocation').val().length == 0 ) {
            $('#locationError').html('This field is required');
            $("label[for='userLocation']").css('color','red');
            flag = false;
        } else {
            flag = true;
        }
        $( '#userLocation' ).keyup(function (e) {
            $('#locationStatusCheck').html('');
            var withoutSpace = $('#userLocation').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $('#userLocation').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $('#locationError').html('This field is required');
                    $("label[for='userLocation']").css('color','red');
                } else {
                    $('#locationError').html('');
                    $("label[for='userLocation']").removeAttr('style');
                    flag = true;    
                }
            }
            if( $('#userLocation').val().length == 0 ) {
                $('#locationError').html('This field is required');
                $("label[for='userLocation']").css('color','red');
                flag = false;
            } else {
                if( withoutSpaceLength == 0 ) {
                    $('#locationError').html('This field is required');
                    $("label[for='userLocation']").css('color','red');
                } else {
                    $('#locationError').html('');
                    $("label[for='userLocation']").removeAttr('style');
                    flag = true;    
                }
            }
        } );
        if( $('#userLocation').val() != '' ) {
            var withoutSpace = $('#userLocation').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $("#label[for='userLocation']").css('color','red');
                $( '#locationError' ).html('This field is required.');
                flag = false;
            } else {
                $('#locationError').html('');
                $("label[for='userLocation']").removeAttr('style');
                flag = true;    
            }
        }
        if( flag == true ) {
            locationSetData['location'] = $scope.location;
            //console.log(JSON.stringify(locationSetData));
            $('.ui-loader').show();
            var responsePromise = $http.post(BASE_URL+"editlocation",JSON.stringify(locationSetData));
            responsePromise.success( function( data, status, headers, config ) {
                $('.ui-loader').hide();
                //console.log(JSON.stringify(data));
                if( data.message == 'user not logged in' ){
                    $state.transitionTo('home.login');
                }
                $('#locationStatusCheck').html(data.message);
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                $('.ui-loader').hide();
                console.log( JSON.stringify( data ) ); 
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
               // alert('server error');
            } );
        }
    };
} )

//+++++++++++++++++++++++++++editaboutme page controller+++++++++++++++++++++

mainApp.controller( "editaboutmeController", function( $scope, $http, $state, $stateParams ) {
    var flag = false;
    var aboutmeData = {};
    aboutmeData['responsetype'] = 'json';
    //console.log(JSON.stringify(aboutmeData));
    var responsePromise = $http.post( BASE_URL+"editaboutme", JSON.stringify( aboutmeData ) );
    responsePromise.success( function( data, status, headers, config ) {
        //console.log( JSON.stringify( data ) );
        $('#aboutme').val(data.data.aboutme);
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
    } );
    $scope.updateAboutMe = function() {
        if( $('#aboutme').val() == '' ) {
            $('#aboutmeError').html('This field is required');
            $("label[for='aboutme']").css('color','red');
            flag = false;
        } else {
            flag = true;
        }
        $( '#aboutme' ).keyup(function (e) {
            if( $( '#aboutmeError' ).html() == 'This field is required' ) {
                $('#aboutmeError').html('');
                $("label[for='aboutme']").removeAttr('style');
                flag = true;
            }
        } );
        if( flag == true ) {
            aboutmeData['aboutme'] = $scope.aboutme;
            //console.log(JSON.stringify(aboutmeData));
            $('.ui-loader').show();
            var responsePromise = $http.post( BASE_URL+"editaboutme", JSON.stringify( aboutmeData ) );
            responsePromise.success( function( data, status, headers, config ) {
                $('.ui-loader').hide();
                //console.log(JSON.stringify(data));
                if( data.message == 'user not logged in' ){
                    $state.transitionTo('home.login');
                }
                $('#aboutmeStatusCheck').html(data.message);
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                console.log( JSON.stringify( data ) ); 
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
               // alert('server error');
            } );
        }
    };
} )

//+++++++++++++++++++++++++++search page controller+++++++++++++++++++++

mainApp.controller( "searchController", function( $scope, $http, $state, $stateParams ) {


} )

//+++++++++++++++++++++++++++viewprofile page controller+++++++++++++++++++++

mainApp.controller( "viewprofileController", function( $scope, $http, $state, $stateParams, $rootScope ) {
    //alert("user id from url"+$stateParams.user_id);
    $scope.playSlide = false;
    $(".hide-content-row").hide();
    $('.save-list-section').hide();
    $("#shortDesc").click ( function() {
        $(".hide-content-row").show();
        $('.save-list-section').show();
        $("#shortDesc").hide();
    } );
    
    $scope.saveContacts = true;
    $scope.saveClick = function () {
        if($scope.saveContacts == true) {
            $scope.saveContacts = false;
        } else {
            $scope.saveContacts = true;
        }
    };
    if( $stateParams.user_id == '' || $stateParams.user_id == null ) {
        //do nothing
    } else {
        //$scope.slides = [];
        var profileData = {};
        profileData['responsetype'] = 'json';
        profileData['uid'] = $stateParams.user_id;
        //console.log(JSON.stringify(profileData));
        var responsePromise = $http.post( BASE_URL+"profile", JSON.stringify( profileData ) );
        responsePromise.success( function( data, status, headers, config ) {
            //console.log(JSON.stringify(data));
            $('#slider_ul').css({
                position:'absolute',
                left: ($(window).width() - $('#slider_ul').outerWidth())/2,
                top: ($(window).height() - $('#slider_ul').outerHeight())/2
            });
            
            $scope.profileInfo = data.data;
            $scope.slides = [
                { image: $scope.profileInfo.profile_image_big, description: 'Image 00' },
                { image: 'img/imgA.jpg', description: 'Image 01' },
                { image: 'img/imgB.jpg', description: 'Image 02' },
                { image: 'img/imgC.jpg', description: 'Image 03' },
                { image: 'img/imgD.jpg', description: 'Image 04' },
                { image: 'img/imgE.jpg', description: 'Image 05' }
            ];


            function getSlide(target, style) {
                var i = target.length;
                return {
                    id: (i + 1),
                    label: 'slide #' + (i + 1),
                    img: $scope.slides[i].image,
                    odd: (i % 2 === 0)
                };
            }

            function addSlide(target, style) {
                target.push(getSlide(target, style));
            };
            $scope.carouselIndex2 = 0;
            function addSlides(target, style, qty) {
                for (var i=0; i < qty; i++) {
                    addSlide(target, style);
                }
            }
            $scope.slides2 = [];
            addSlides($scope.slides2, 'sports', 6);

            $scope.playSlider = function () {
                if ( $scope.playSlide == true ) {
                    $scope.playSlide = false;
                } else {
                    $scope.playSlide = true;
                }
                
            };
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
           // alert('server error');
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );

    }
    
    $(".slider-popup-frame").hide();
    $scope.openPopup = function () {
        //console.log('clicked on img');
        $(".slider-popup-frame").show();
    };
    $scope.closePopup = function () {
        $(".slider-popup-frame").hide();
    };
    $scope.discription = function () {
        $(".hide-content-row").hide();
        $("#shortDesc").show();
        $(".save-list-section").hide();
    };
    /** angular slider**/
} );
/*.animation('.slide-animation', function () {
return {
    beforeAddClass: function (element, className, done) {
        var scope = element.scope();

        if (className == 'ng-hide') {
            var finishPoint = element.parent().width();
            if(scope.direction !== 'right') {
                finishPoint = -finishPoint;
            }
            TweenMax.to(element, 0.5, {
                left: finishPoint, onComplete: done 
            });
        } else {
            done();
        }
    },
        removeClass: function (element, className, done) {
            var scope = element.scope();

            if (className == 'ng-hide') {
                element.removeClass('ng-hide');

                var startPoint = element.parent().width();
                if(scope.direction === 'right') {
                    startPoint = -startPoint;
                }

                TweenMax.fromTo(element, 0.5, { left: startPoint }, {left: 0, onComplete: done });
            }
            else {
                done();
            }
        }
    };
    /** angular slider end**/
/*} );*/
