//+++++++++++++++++++++++++++proUpgrade page controller+++++++++++++++++++++

mainApp.controller( "proUpgradeController", function( $scope, $http, $state, $rootScope, $location ) {
    var flag = false;
    Stripe.setPublishableKey('pk_test_0V7OYkfVgcuIXhVj1INmpePj');
    function stripeResponseHandler(status, response) {
        //alert(3);
        if (response.error) {
            // re-enable the submit button
            $('.submit-button').removeAttr("disabled");
            // show the errors on the form
            $(".payment-errors").html(response.error.message);
        } else {
            var form$ = $("#proUpgrade_form");
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            form$.append("<input type='hidden' id='token_upgrade' name='stripeToken' value='" + token + "' />");
            // and submit
            proUpgradeSubmit();
        }
    }
    $scope.upgradeSubmit = function () {
        $('#token_upgrade').remove();
        var new_val = "";
        var proFormDiv = document.getElementById('proUpgrade_form');
        var input_id = ''; 
        var elementsLength = proFormDiv.getElementsByTagName('input').length;
        //alert(1);
        for (var i=0; i<parseInt(elementsLength); i++) {

            input_id = proFormDiv.getElementsByTagName('input')[i].id;
            new_val = proFormDiv.getElementsByTagName('input')[i].value;
            $( "label[for='"+input_id+"']" ).removeAttr('style');
            document.getElementsByTagName("span")[i].innerHTML = '';
            if( new_val == '' ) {
                var flag = false;
                document.getElementsByTagName("span")[i].innerHTML = 'This field is required.';
                $("label[for='"+input_id+"']").css( 'color', 'red' );
            }
            //alert(i);
        }
        //alert("before for");
        if($('#register_expiremonth').val()==''){
            $('#expiremonth_error').html( 'This field is required.' );
            $('#expiremonth_label').css( 'color', 'red' );
            var flag = false;
        }
        if($('#register_expireyear').val()==''){
            $('#expireyear_error').html('This field is required.');
            $('#expireyear_label').css( 'color', 'red' );
            var flag = false;
        }
        if($('#upgrade_country').val()==''){
            $('#upgrade_country_error').html('This field is required.');
            $('#upgrade_country_label').css( 'color', 'red' );
            var flag = false;
        }
        $( '#register_fullname' ).keyup(function (e) {
            if( $( '#fullname_error' ).html() == 'This field is required.' ) {
                $( '#fullname_error' ).html( 'Please enter at least 3 characters.' );
            }
            if( $('#register_fullname').val().length >= 3 ) {
                    $( '#fullname_error' ).html('');
                    $( '#fullname_label' ).removeAttr( 'style' );
            } else {
                $( '#fullname_error' ).html( 'Please enter at least 3 characters.' );
                $('#fullname_label').css( 'color', 'red' );
            }
        } );
        $( '#register_cardnumber' ).keyup( function (e) {
            if( $( '#cardnumber_error' ).html() == 'This field is required.' ) {
                $( '#cardnumber_error' ).html( 'Please enter at least 16 characters.' );
            }
            if( $('#register_cardnumber').val().length >= 16 ) {
                    $( '#cardnumber_error' ).html('');
                    $( '#cardnumber_label' ).removeAttr('style');
            } else {
                $( '#cardnumber_error' ).html( 'Please enter at least 16 characters.' );
                $('#cardnumber_label').css( 'color', 'red' );
            }
        } );

        $( '#register_cvv2' ).keyup( function (e) {
            if( $( '#cvv2_error' ).html() == 'This field is required.' ) {
                $( '#cvv2_error' ).html('Please enter at least 3 characters.');
            }
            if( $('#register_cvv2').val().length >= 3 ) {
                    $( '#cvv2_error' ).html('');
                    $( '#cvv2_label' ).removeAttr('style');
            } else {
                $( '#cvv2_error' ).html('Please enter at least 3 characters.');
                $( '#cvv2_label' ).css('color','red');
            }
        } );

        $( '#upgrade_zipcode' ).keyup(function (e) {
            if( $( '#upgrade_zipcode_error' ).html() == 'This field is required.' ) {
                $( '#upgrade_zipcode_error' ).html('Please enter at least 5 characters.');
            }
            if( $('#upgrade_zipcode').val().length >= 5 ) {
                    $( '#upgrade_zipcode_error' ).html('');
                    $( '#upgrade_zipcode_label' ).removeAttr('style');
            } else {
                $( '#upgrade_zipcode_error' ).html('Please enter at least 5 characters.');
                $( '#upgrade_zipcode_label' ).css('color','red');
            }
        } );

        $( '#register_expiremonth' ).change(function() {
            if($( '#register_expiremonth' ).val() == ''){
                $( '#expiremonth_error' ).html('This field is required.');
                $('#expiremonth_label').css('color','red');
            } else {
                $( '#expiremonth_error' ).html('');
                $( '#expiremonth_label' ).removeAttr('style');
            }
        } );

        $( '#register_expireyear' ).change(function() {
            if($( '#register_expiremonth' ).val() == ''){
                $( '#expireyear_error' ).html('This field is required.');
                $('#expireyear_label').css('color','red');
            } else {
                $( '#expireyear_error' ).html('');
                $( '#expireyear_label' ).removeAttr('style');
            }
        } );

        $( '#upgrade_country' ).change(function() {
            if($( '#upgrade_country' ).val() == ''){
                $( '#upgrade_country_error' ).html('This field is required.');
                $('#upgrade_country_label').css('color','red');
            } else {
                $( '#upgrade_country_error' ).html('');
                $( '#upgrade_country_label' ).removeAttr('style');
            }
        } );

        if( $('#register_fullname').val() != '' && $('#register_fullname').val().length < 3) {
            var flag = false;
            $( '#fullname_error' ).html('Please enter at least 3 characters.');
            $( '#fullname_label' ).css('color','red');
        }

        if( $('#register_cardnumber').val() != '' && $('#register_cardnumber').val().length < 16) {
            var flag = false;
            $( '#cardnumber_error' ).html('Please enter at least 16 characters.');
            $( '#cardnumber_label' ).css('color','red');
        }

        if( $('#register_cvv2').val() != '' && $('#register_cvv2').val().length < 3) {
            var flag = false;
            $( '#cvv2_error' ).html('Please enter at least 3 characters.');
            $( '#cvv2_label' ).css('color','red');
        }
        
        if( $('#upgrade_zipcode').val() != '' && $('#upgrade_zipcode').val().length < 5) {
            var flag = false;
            $( '#upgrade_zipcode_error' ).html('Please enter at least 5 characters.');
            $( '#upgrade_zipcode_label' ).css('color','red');
        }
        if ( $( '#expiremonth_error' ).html() == '' && $( '#fullname_error' ).html() == '' && $( '#cvv2_error' ).html() == '' && $( '#expireyear_error' ).html() == '' && $( '#cardnumber_error' ).html() == '' && $( '#upgrade_zipcode_error' ).html() == '' && $( '#upgrade_country_error' ).html() == '' ) {
            flag = true;
        }
        if( flag == true ) {
            //alert(2);
            // disable the submit button to prevent repeated clicks
            $('.submit-button').attr("disabled", "disabled");
            // createToken returns immediately - the supplied callback submits the form if there are no errors
            Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler );
        } 
        //return false; // submit from callback
        
    };
    function proUpgradeSubmit() {
        //alert(4);
        var getUpgradeData = {};
        getUpgradeData['full_name'] = $( '#register_fullname' ).val();
        getUpgradeData['card_number'] = $( '#register_cardnumber' ).val();
        getUpgradeData['expire_month'] = $( '#register_expiremonth' ).val();
        getUpgradeData['expire_year'] = $( '#register_expireyear' ).val();
        getUpgradeData['cvv_number'] = $( '#register_cvv2' ).val();
        getUpgradeData['zipcode'] = $( '#upgrade_zipcode' ).val();
        getUpgradeData['country'] = $( '#upgrade_country' ).val();
        getUpgradeData['token'] = $( '#token_upgrade' ).val();
        if( $rootScope.previousState == 'home.settings' && $rootScope.currentState == 'home.updatepayment' ) {
            console.log('========changed=========');
            getUpgradeData['s'] = "updatepay";
        } 
        getUpgradeData['responsetype'] = 'json';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        console.log( JSON.stringify( getUpgradeData ) ); 
        var responsePromise = $http.post( BASE_URL+"upgrade",JSON.stringify( getUpgradeData )  );
        responsePromise.success( function( data, status, headers, config ) {
            //alert(5);
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( data.status == 'success' ) {
                $( '#upgradeStatus' ).html( data.message );
            } else {
                $( '#upgradeStatus' ).html( data.message );
            }
            $( '.submit-button' ).removeAttr("disabled");
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
            console.log( JSON.stringify( data ) ); 
            $('.submit-button').removeAttr("disabled");
        } );
    }
} )

//+++++++++++++++++++++++++++Learn more page controller+++++++++++++++++++++

mainApp.controller( "learnmoreController", function( $scope, $http, $timeout, $state ) {
    $scope.proDashboardLink = function( $event ) {
        $state.transitionTo('home.prodashboard');
    };
} )

//+++++++++++++++++++++++++++prodashboard page controller+++++++++++++++++++++

mainApp.controller( "prodashboardController", function( $scope, $http, $timeout, $state ) {
    $scope.verifyIdentity = function( $event ) {
        $state.transitionTo('home.verifyidentity');
    };
    $scope.paymentDetails = function( $event ) {
        $state.transitionTo('home.paymentoption');
    };
    $scope.accpetTerms = function( $event ) {
        $state.transitionTo('home.acceptterms');
    };

    //get Pro data
    var getProDashboardData = {};
    getProDashboardData['responsetype'] = 'json';
    //console.log('Pro data=>'+JSON.stringify(getProDashboardData));
    var responsePromise = $http.post(BASE_URL+"pro/prodashboard",JSON.stringify(getProDashboardData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log('Pro response => '+JSON.stringify(data));
        localStorage.setItem("proDashboardData",JSON.stringify(data));
        if( data.data.pv_terms == 1 ) {
            $('#accept_terms_btn').addClass('orangeBlock');
            $('#accept_terms_span').addClass('glyphicon glyphicon-time');
            $scope.protermsData = '(Pending review)';
            $("#accept_terms_btn").unbind("click");
        } else if( data.data.pv_terms == 2 ) {
            $('#accept_terms_btn').addClass('greenBlock');
            $('#accept_terms_span').addClass('glyphicon glyphicon-ok');
            $scope.protermsData = '(Completed)';
            $("#accept_terms_btn").unbind("click");
        } else if( data.data.pv_terms == -1 ){
            $('#accept_terms_btn').addClass('redBlock');
            $('#accept_terms_span').addClass('glyphicon glyphicon-warning-sign');
            $scope.protermsData = '(Action required)';
        } else {
            $('#accept_terms_span').addClass('glyphicon glyphicon-chevron-right');
            $scope.protermsData = '';
        }

        if( data.data.pv_idverify == 1) {
            $('#verify_identity_btn').addClass('orangeBlock');
            $('#verify_identity_span').addClass('glyphicon glyphicon-time');
            $scope.proidverifyData = '(Pending review)';
            $("#verify_identity_btn").unbind("click");
        } else if( data.data.pv_idverify == 2 ) {
            $('#verify_identity_btn').addClass('greenBlock');
             $('#verify_identity_span').addClass('glyphicon glyphicon-ok');
            $scope.proidverifyData = '(Completed)';
            $("#verify_identity_btn").unbind("click");
        } else if( data.data.pv_idverify == -1 ){
            $('#verify_identity_btn').addClass('redBlock');
             $('#verify_identity_span').addClass('glyphicon glyphicon-warning-sign');
            $scope.proidverifyData = '(Action required)';
        } else {
            $('#verify_identity_span').addClass('glyphicon glyphicon-chevron-right');
            $scope.proidverifyData = '';
        }

        if( data.data.pv_payinfo == 1) {
            $('#payment_details_btn').addClass('orangeBlock');
            $('#payment_details_span').addClass('glyphicon glyphicon-time');
            $scope.propayinfoData = '(Pending review)';
            $("#payment_details_btn").unbind("click");
        } else if( data.data.pv_payinfo == 2 ) {
            $('#payment_details_btn').addClass('greenBlock');
            $('#payment_details_span').addClass('glyphicon glyphicon-ok');
            $scope.propayinfoData = '(Completed)';
            $("#payment_details_btn").unbind("click");
        } else if( data.data.pv_payinfo == -1 ){
            $('#payment_details_btn').addClass('redBlock');
            $('#payment_details_span').addClass('glyphicon glyphicon-warning-sign');
            $scope.propayinfoData = '(Action required)';
        } else {
            $('#payment_details_span').addClass('glyphicon glyphicon-chevron-right');
            $scope.propayinfoData = '';
        }

    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
} )

//+++++++++++++++++++++++++++acceptterms page controller+++++++++++++++++++++

mainApp.controller( "accepttermsController", function( $scope, $http, $timeout, $state ) {
    $scope.acceptTermsLink = function( $event ) {
        var accpetTermData = {};
        accpetTermData['accepted'] = 1;
        accpetTermData['responsetype'] = 'json';
        //console.log(JSON.stringify(accpetTermData));
        var responsePromise = $http.post(BASE_URL+"pro/acceptterms",JSON.stringify(accpetTermData));
        responsePromise.success( function( data, status, headers, config ) {
            //console.log(JSON.stringify(data));
            $state.transitionTo('home.accepttermscomplete');
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    
} )

//+++++++++++++++++++++++++++accepttermsComplete page controller+++++++++++++++++++++

mainApp.controller( "accepttermscompleteController", function( $scope, $http, $timeout, $state ) {
    $scope.completeLink = function( $event ) {
        $state.transitionTo('home.prodashboard');
    };
} )

//+++++++++++++++++++++++++++paymentoption page controller+++++++++++++++++++++

mainApp.controller( "paymentoptionController", function( $scope, $http, $timeout, $state ) {
   var flag = false;
    $scope.paymentAddrSubmit = function( $event ) {
        var new_val = "";
        var paymentOptionDiv = document.getElementById('paymentoption_form');
        var input_id = ''; 
        var elementsLength = paymentOptionDiv.getElementsByTagName('input').length;
        if( $scope.output == true ) {
            $('#output').val('Y');
        }  
        for (var i=0; i<parseInt(elementsLength); i++) {
            input_id = paymentOptionDiv.getElementsByTagName('input')[i].id;
            new_val = paymentOptionDiv.getElementsByTagName('input')[i].value;
            $("label[for='"+input_id+"']").removeAttr('style');
            document.getElementsByTagName("span")[i].innerHTML = '';
            if( i == 2 ) {
                continue;
            }
            if( new_val == '' ) {
                document.getElementsByTagName("span")[i].innerHTML = 'This field is required.';
                $("label[for='"+input_id+"']").css('color','red');
            }
            if( i == elementsLength-1 ){
                if($('#output').val() == "N"){
                    $('#output_error').html('This field is required.');
                    $('#output_label').css('color','red');
                } 
            }
        }

        if( $('#state').val()=='' ) {
            $('#state_error').html('This field is required.');
            $('#state_label').css('color','red');
        }
        if( $('#country').val()=='' ) {
            $('#country_error').html('This field is required.');
            $('#country_label').css('color','red');
        }

        $( '#fullname' ).keyup(function (e) {
            if( $( '#fullname_error1' ).html() == 'This field is required.' ) {
                $( '#fullname_error1' ).html('Please enter at least 2 characters.');
            }
            if( $('#fullname').val().length >= 2) {
                    $( '#fullname_error1' ).html('');
                    $( '#fullname_label1' ).removeAttr('style');
            } else {
                $( '#fullname_error1' ).html('Please enter at least 2 characters.');
                $( '#fullname_label1' ).css('color','red');
            }
        } );

        $( '#address1' ).keyup(function (e) {
            if( $( '#address1_error' ).html() == 'This field is required.' ) {
                $( '#address1_error' ).html('Please enter at least 2 characters.');
            }
            if( $('#address1').val().length >= 2 ) {
                    $( '#address1_error' ).html('');
                    $( '#address1_label' ).removeAttr('style');
            } else {
                $( '#address1_error' ).html('Please enter at least 2 characters.');
                $('#address1_label').css('color','red');
            }
        } );

        $( '#city' ).keyup(function (e) {
            if( $( '#city_error' ).html() == 'This field is required.' ) {
                $( '#city_error' ).html('Please enter at least 2 characters.');
            }
            if( $('#city').val().length >= 2 ) {
                    $( '#city_error' ).html('');
                    $( '#city_label' ).removeAttr('style');
            } else {
                $( '#city_error' ).html('Please enter at least 2 characters.');
                $('#city_label').css('color','red');
            }
        } );

        $( '#state' ).change(function() {
            if($( '#state' ).val() == ''){
                $( '#state_error' ).html('This field is required.');
                $('#state_label').css('color','red');
            } else {
                $( '#state_error' ).html('');
                $( '#state_label' ).removeAttr('style');
            }
        } );

        $( '#country' ).change(function() {
            if($( '#country' ).val() == ''){
                $( '#country_error' ).html('This field is required.');
                $('#country_label').css('color','red');
            } else {
                $( '#country_error' ).html('');
                $( '#country_label' ).removeAttr('style');
            }
        } );

        $( '#zipcode' ).keyup(function (e) {
            if( $( '#zipcode_error' ).html() == 'This field is required.' ) {
                $( '#zipcode_error' ).html('Please enter at least 5 characters.');
            }
            if( $('#zipcode').val().length >= 5 ) {
                    $( '#zipcode_error' ).html('');
                    $( '#zipcode_label' ).removeAttr('style');
            } else {
                $( '#zipcode_error' ).html('Please enter at least 5 characters.');
                $( '#zipcode_label' ).css('color','red');
            }
        } );

        $('#output').change(function() {
            if($(this).is(":checked")) {
                $( '#output_error' ).html('');
                $( '#output_label' ).removeAttr('style');
            } else {
                $( '#output_error' ).html('This field is required.');
                $( '#output_label' ).css('color','red');
            }
        });

        if( $('#fullname').val() != '' && $('#fullname').val().length < 2) {
            
                $( '#fullname_error1' ).html('Please enter at least 2 characters.');
                $( '#fullname_label1' ).css('color','red');
        }

        if( $('#address1').val() != '' && $('#address1').val().length < 2) {
            
                $( '#address1_error' ).html('Please enter at least 2 characters.');
                $( '#address1_label' ).css('color','red');
        }

        if( $('#city').val() != '' && $('#city').val().length < 2) {
            
                $( '#city_error' ).html('Please enter at least 2 characters.');
                $( '#city_label' ).css('color','red');
        }

        if( $('#zipcode').val() != '' && $('#zipcode').val().length < 5) {
            
                $( '#zipcode_error' ).html('Please enter at least 5 characters.');
                $( '#zipcode_label' ).css('color','red');
        }

        if ( $( '#state_error' ).html() == '' && $( '#zipcode_error' ).html() == '' && $( '#country_error' ).html() == '' && $( '#city_error' ).html() == '' && $( '#address1_error' ).html() == '' && $( '#fullname_error1' ).html() == '' && $( '#output_error' ).html()=='' ) {
            flag = true;
        }

        var paymentOptionData = {};
        paymentOptionData['fullname'] = $scope.fullname;
        paymentOptionData['address1'] = $scope.address1;
        paymentOptionData['city'] = $scope.city;
        paymentOptionData['state'] = $scope.state;
        paymentOptionData['country'] = $scope.country;
        paymentOptionData['zipcode'] = $scope.zipcode;
        paymentOptionData['output'] = $scope.output;
        paymentOptionData['address2'] = $scope.address2;
        paymentOptionData['responsetype'] = 'json';
        if( flag == true ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            //console.log( JSON.stringify( paymentOptionData ) ); 
            var responsePromise = $http.post(BASE_URL+"pro/paymentoption",JSON.stringify(paymentOptionData));
            responsePromise.success( function( data, status, headers, config ) {
                //console.log( JSON.stringify( data ) ); 
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( data.status == 'success' ) {
                    $state.transitionTo('home.paymentoptcomplete');
                } else {
                    $('#paymentOptionStatus').html( data.message );
                }
            } );
            responsePromise.error( function ( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
                console.log( JSON.stringify( data ) ); 
            } );
        }
    };
} )


//+++++++++++++++++++++++++++verifyidentity page controller+++++++++++++++++++++

mainApp.controller( "verifyidentityController", function( $scope, $http, $timeout, $state ) {
    $scope.div_visible = true;
    $scope.takePhotoOpt = false;

    $scope.uploadVerifyPhoto = function () {
        $scope.takePhotoOpt = true;
    };
    
    $scope.cancelPhoto = function () {
        $scope.takePhotoOpt = false;
    };

    function onPhotoDataSuccess(imageData) {
        var profileImage = document.getElementById('verify_image');
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageStringVerify = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function () {
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, allowEdit: true, correctOrientation: true,
        destinationType: destinationType.DATA_URL });
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: pictureSource.PHOTOLIBRARY });
    };
    
    // Called if something bad happens.
    function onFail(message) {
        //alert('Failed because: ' + message);
        $scope.takePhotoOpt = false;
        $scope.div_visible = true;
    }

    //when success upload
    $scope.saveVerifyPic = function(){
        var updateVerifyData = {};
        updateVerifyData['responsetype'] = "json";
        updateVerifyData['image'] = imageStringVerify;
        //console.log(JSON.stringify(updateVerifyData));
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var responsePromise = $http.post(BASE_URL+"pro/uploadphotoid",JSON.stringify(updateVerifyData));
        responsePromise.success( function( data, status, headers, config ) {
            //console.log(JSON.stringify(data));
            $('#uploadVerify_id').html('Profile photo has been saved');
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };

    $scope.cancelUpload = function ( $event) {
        $scope.div_visible = true;
    };
    
})

//+++++++++++++++++++++++++++prosettings page controller+++++++++++++++++++++

mainApp.controller( "prosettingsController", function( $scope, $http, $timeout, $state ) {
    $scope.setCall = function(){
        $state.transitionTo('home.setrates');
    };
    $scope.viewEarning = function(){
        $state.transitionTo('home.stats');
    };
} )

//+++++++++++++++++++++++++++setrates page controller+++++++++++++++++++++

mainApp.controller( "setratesController", function( $scope, $http, $timeout, $state ) {
    $scope.per_minute_select = [
        {id:'0.00', value:'$0 per min'},
        {id:'0.49', value:'$0.49 per min'},
        {id:'0.99', value:'$0.99 per min'},
        {id:'1.49', value:'$1.49 per min'},
        {id:'1.99', value:'$1.99 per min'},
        {id:'2.49', value:'$2.49 per min'},
        {id:'2.99', value:'$2.99 per min'},
        {id:'3.49', value:'$3.49 per min'}
    ];
    //$scope.perMinuteCode = $scope.per_minute_select[0];

    $scope.per_text_select = [
        {id:'0.00', value:'$0 per txt msg'},
        {id:'0.10', value:'$0.10 per txt msg'},
        {id:'0.15', value:'$0.15 per txt msg'},
        {id:'0.20', value:'$0.20 per txt msg'},
        {id:'0.25', value:'$0.25 per txt msg'},
        {id:'0.30', value:'$0.30 per txt msg'},
        {id:'0.35', value:'$0.35 per txt msg'},
        {id:'0.40', value:'$0.40 per txt msg'},
        {id:'0.45', value:'$0.45 per txt msg'},
        {id:'0.50', value:'$0.50 per txt msg'}
    ];
    //$scope.perTextCode = $scope.per_text_select[0];
    var rateData = {};
    rateData['responsetype'] = 'json';
    //console.log( JSON.stringify( rateData ) ); 
    var responsePromise = $http.post(BASE_URL+"pro/setrates",JSON.stringify(rateData));
    responsePromise.success( function( data, status, headers, config ) {
        $scope.perMinuteCode = data.data.call_rate;
        $scope.perTextCode = data.data.text_rate;
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log( JSON.stringify( data ) ); 
    } );
    $scope.updateRates = function(){
        var setrates = {};
        setrates['perminute_rate'] = $scope.perMinuteCode;
        setrates['pertext_rate'] = $scope.perTextCode;
        setrates['responsetype'] = 'json';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        //console.log(JSON.stringify(setrates));
        var responsePromise = $http.post(BASE_URL+"pro/setrates",JSON.stringify(setrates));
        responsePromise.success( function( data, status, headers, config ) {
            //console.log(JSON.stringify(data));
            $('#rate_status').html(data.message);
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            console.log( JSON.stringify( data ) ); 
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    
} )

//+++++++++++++++++++++++++++stats page controller+++++++++++++++++++++

mainApp.controller( "statsController", function( $scope, $http, $timeout, $state, $filter ) {
    $scope.currPeriodSelect = [
        {id:'1', value:'Current period - 9/1 to 9/15/2014'},
        {id:'ad', value:'8/1 to 8/15/2014'},
        {id:'ai', value:'8/16 to 8/31/2014'}
    ];
    $scope.period_select = $scope.currPeriodSelect[0];
    $scope.date = new Date();
    var statsData = {};
    statsData['responsetype'] = 'json';
    //console.log(JSON.stringify(statsData));
    var responsePromise = $http.post(BASE_URL+"pro/stats",JSON.stringify(statsData));
    responsePromise.success( function( data, status, headers, config ) {
        //console.log(JSON.stringify(data));
        $scope.stats = data.data;
        var curr_charge = parseFloat(data.data.calls_currentperiod.call_charge) + parseFloat(data.data.msg_currentperiod.msg_charge)+parseFloat(50.00);
        $scope.totalCurrCharge = curr_charge.toFixed(2); 
        var today_charge = parseFloat(data.data.calls_today.call_charge) + parseFloat(data.data.msg_today.msg_charge)+parseFloat(10.00);
        $scope.totalTodayCharge = today_charge.toFixed(2); 
        var call_time = data.data.calls_currentperiod.call_secs/60;
        $scope.callTime = Math.floor(call_time);
        var curr_call = parseFloat(data.data.calls_currentperiod.call_charge);
        $scope.currCallCharge = curr_call.toFixed(2); 
    } );
    responsePromise.error( function ( data, status, headers, config ) {
        console.log(JSON.stringify(data));
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );  
    $scope.initial = 'Sep 1';
    $scope.final1 = 'Sep 15, 2014';
    $scope.currPeriodChange = function(){
        var selected_val = $scope.period_select.value;
        var dateExp = selected_val.split(" ");
        var initial_date = dateExp[0];
        var final_date = dateExp[2];
        var initial_date_exp = initial_date.split("/");
        var final_date_exp = final_date.split("/");
    };
} )

//+++++++++++++++++++++++++++how it works page controller+++++++++++++++++++++

mainApp.controller( "howitworksCtrl", function( $scope, $http, $timeout, $state, $filter ) {
    $scope.goBack = function(){
        window.history.back();
    };
} )
